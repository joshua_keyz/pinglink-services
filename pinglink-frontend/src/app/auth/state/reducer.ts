import { createReducer, on } from "@ngrx/store";
import { clearErrors, emailVerificationError, resetPasswordSuccess, sendForgotPWDError, signInError, signInSuccess, signUpError, signUpRequest, signUpSuccess, verifyCodeError } from "./actions";
import { AuthStateModel } from "../models/auth-state.model";

export const initialState: AuthStateModel = {
  token: '',
  signUpError: '',
  signInError: '',
  forgotPasswordError: '',
};

export const exampleReducer = createReducer(
  initialState,
  on(signUpSuccess, signInSuccess, resetPasswordSuccess, (state, action) => ({...state, token: action.payload.token})),
  on( emailVerificationError, (state, action) => ({...state, signUpError: action.payload.error})),
  on( signInError, (state, action) => {
    return ({...state, signInError: action.payload.error})
  }),
  on(verifyCodeError, (state, action) => ({
    ...state,
    verifyCodeError: action.payload.error
  })),
  on(sendForgotPWDError, (state, action) => ({...state, forgotPasswordError: action.payload.error})),
  on( signUpError, (state, action) => ({...state, signUpError: action.payload.error})),
  on(clearErrors, state => ({...state, signUpError: '', signInError: '', forgotPasswordError: ''})),
);
import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { AuthService } from "../services/auth.service";
import * as authActions from './actions';
import { catchError, map, of, switchMap, tap } from "rxjs";
import { SignUpSuccessModel } from "../models/signup-success.model";
import { Router } from "@angular/router";
import { VerificationSuccessModel } from "../models/verification-success.model";
import { SignInSuccessModel } from "../models/signin-success.model";
import { ForgotPasswordResponseModel } from "../models/forgot-password-response.model";
import { VerificationCounterService } from "../services/verification-counter.service";

@Injectable()
export class AuthEffects {

  registerUser$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.signUpRequest),
    switchMap((action) => this.authService.registerUser(action.payload).pipe(
      switchMap((response: SignUpSuccessModel) => [authActions.signUpSuccess({payload: response}), authActions.clearErrors()]),
      catchError((error) => of(authActions.signUpError({payload: error.error})))
    ))
  ));
  
  signInUser$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.signInRequest),
    switchMap((action) => this.authService.signInUser(action.payload).pipe(
      map((response: SignInSuccessModel) => authActions.signInSuccess({payload: response})),
      catchError((error) => of(authActions.signInError({payload: error.error})))
    ))
  ));

  requestPasswordReset$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.sendForgotPWDRequest),
    switchMap((action) => this.authService.requestResetPassword(action.payload).pipe(
      map((response: ForgotPasswordResponseModel) => authActions.sendForgotPWDSuccess({payload: response})),
      catchError(error => of(authActions.sendForgotPWDError({payload: error.error})))
    ))
  ));
  sendForgotPasswordSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.sendForgotPWDSuccess),
    tap((action) => {
      localStorage.setItem('resetPasswordToken', action.payload.data.resetPasswordToken);
      this.router.navigate(['auth', 'verify-code'],{queryParams: {
        verificationToken: action.payload.data.resetPasswordToken
      }})
    })
  ), {dispatch: false});
  goToVerificationPage$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.signUpSuccess),
    tap(action => {
      localStorage.setItem('pinglink-auth-token', action.payload.token);
      localStorage.setItem('lastVerificationSent', JSON.stringify(Date.now()))
      this.router.navigateByUrl('/auth/verify-email')
    }),
  ), {dispatch: false});
  verifyEmail$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.emailVerificationRequest),
    switchMap(action => this.authService.verifyEmail(action.payload).pipe(
      map((response: VerificationSuccessModel) => authActions.emailVerificationSuccess({payload: response})),
      catchError(error => of(authActions.emailVerificationError({payload: error.error})))
    )),
  ));
  verifyCode$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.verifyCodeRequest),
    switchMap(action => this.authService.verifyCode(action.payload.verificationCode).pipe(
      map((response) => authActions.verifyCodeSuccess({payload: {success: true, resetToken: action.payload.verificationCode}})),
      catchError(error => of(authActions.verifyCodeError({payload: error.error})))
    )),
  ));
  resendVerificationCode$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.resendVerificationCode),
    switchMap(action => this.authService.resendVerificationCode().pipe(
      map((response) => authActions.resendVerificationCodeSuccess()),
      catchError(error => of(authActions.resendVerificationCodeError()))
    )),
  ));
  resendVerificationCodeSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.resendVerificationCodeSuccess),
    tap(action => {
      localStorage.setItem('lastVerificationSent', JSON.stringify(Date.now()))
      this.counterService.initTimer()
    }),
  ), {dispatch: false});
  // resendVerificationCodeError$ = createEffect(() => this.actions$.pipe(
  //   ofType(authActions.resendVerificationCodeError),
  //   tap(action => this.authService.resendVerificationCode().pipe(
  //     map((response) => authActions.resendVerificationCodeSuccess()),
  //     catchError(error => of(authActions.resendVerificationCodeError()))
  //   )),
  // ), {dispatch: false});
  resetPasswordRequest$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.resetPasswordRequest),
    switchMap(action => this.authService.resetPassword(action.payload.password, action.payload.resetToken).pipe(
      map((response) => authActions.resetPasswordSuccess({payload: response})),
      catchError(error => of(authActions.resetPasswordError({payload: error.error})))
    )),
  ));

  verifyCodeSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.verifyCodeSuccess),
    tap(action => {
      this.router.navigate(['/auth', 'reset-password'],  { queryParams: {
        resetToken: action.payload.resetToken
      }, queryParamsHandling: 'merge'});
    }),
  ), {dispatch: false})
  resetPasswordSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.resetPasswordSuccess),
    tap(action => {
      localStorage.setItem('pinglink-auth-token', action.payload.token);
      this.router.navigate(['/home']);
    }),
  ), {dispatch: false})

  goToHomePage$ = createEffect(() => this.actions$.pipe(
    ofType(authActions.emailVerificationSuccess, authActions.signInSuccess),
    tap(action => {
      localStorage.setItem('pinglink-auth-token', action.payload.token);
      this.router.navigateByUrl('/home')
    })
  ), {dispatch: false})

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private counterService: VerificationCounterService
  ) {}
}
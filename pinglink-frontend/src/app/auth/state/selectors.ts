import { createSelector } from "@ngrx/store";
import { AuthStateModel } from "../models/auth-state.model";
import { AppStateModel } from "src/app/models/app-state.model";

export const selectFeature = (state: AppStateModel) => state.auth;
export const selectAuthError = createSelector(
  selectFeature,
  (state: AuthStateModel) => state.signUpError
);
export const selectSignInError = createSelector(
  selectFeature,
  (state: AuthStateModel) => state.signInError
);
export const selectForgotPasswordError = createSelector(
  selectFeature,
  (state: AuthStateModel) => state.forgotPasswordError
);
export const selectSignUpError = createSelector(
  selectFeature,
  (state: AuthStateModel) => state.signUpError
);
export const selectAuthToken = createSelector(
  selectFeature,
  (state: AuthStateModel) => state.token
);
export const verifyCodeError = createSelector(
  selectFeature,
  (state: AuthStateModel) => state.verifyCodeError
);
import { createAction, props } from "@ngrx/store";
import { SignUpRequestModel } from "../models/signup-request.model";
import { SignUpSuccessModel } from "../models/signup-success.model";
import { SignUpErrorModel } from "../models/signup-error.model";
import { VerificationRequestModel } from "../models/verification-request.model";
import { VerificationSuccessModel } from "../models/verification-success.model";
import { VerificationErrorModel } from "../models/verification-error.model";
import { SignInRequestModel } from "../models/signin-request.model";
import { SignInSuccessModel } from "../models/signin-success.model";
import { SignInErrorModel } from "../models/signin-error.model";
import { ForgotPasswordRequestModel } from "../models/forgot-password-request.model";
import { ForgotPasswordResponseModel } from "../models/forgot-password-response.model";
import { ForgotPasswordErrorModel } from "../models/forgot-password-error.model";
import { ResetPasswordRequestModel } from "../models/reset-password-request.model";
import { ResetPasswordSuccessModel } from "../models/reset-password-success.model";
import { ResetPasswordErrorModel } from "../models/reset-password-error.model";

export const signInRequest = createAction(
  '[Sign In Component] Sign in Request',
  props<{payload: SignInRequestModel}>()
);
export const signInSuccess = createAction(
  '[Auth Effects] Sign in Success',
  props<{payload: SignInSuccessModel}>()
);
export const signInError = createAction(
  '[Auth Effects] Sign in Error',
  props<{payload: SignInErrorModel}>()
);

export const sendForgotPWDRequest = createAction(
  '[Forgot Password Component] Send Forgot Password Request',
  props<{payload: ForgotPasswordRequestModel}>()
);
export const sendForgotPWDSuccess = createAction(
  '[Auth Effects] Send Forgot Password Success',
  props<{payload: ForgotPasswordResponseModel}>()
);
export const sendForgotPWDError = createAction(
  '[Auth Effects] Send Forgot Password Error',
  props<{payload: ForgotPasswordErrorModel}>()
);

export const signUpRequest = createAction(
  '[Sign In Component] Sign Up Request',
  props<{payload: SignUpRequestModel}>()
);
export const signUpSuccess = createAction(
  '[Auth Effects Service] Sign Up Success',
  props<{payload: SignUpSuccessModel}>()
);
export const signUpError = createAction(
  '[Auth Effects Service] Sign Up Error',
  props<{payload: SignUpErrorModel}>()
);
export const verifyCodeRequest = createAction(
  '[VerifyCodeComponent] Verify Code Request', 
  props<{payload: {
    verificationCode: string
  }}>()
)
export const verifyCodeSuccess = createAction(
  '[Auth Effects Service] Verify Code Success', 
  props<{payload: {
    success: boolean,
    resetToken: string,
  }}>()
)
export const verifyCodeError = createAction(
  '[Auth Effects Service] Verify Code Error', 
  props<{payload: {
    success: boolean,
    error: string
  }}>()
)
export const clearErrors = createAction(
  '[Sign In Component] Clear Errors',
);
export const emailVerificationRequest = createAction(
  '[Email Verification Component] Verify Email Request',
  props<{payload: VerificationRequestModel}>()
);
export const emailVerificationSuccess = createAction(
  '[Auth Effects Service] Verify Email Success',
  props<{payload: VerificationSuccessModel}>()
);
export const resetPasswordRequest = createAction(
  '[Reset Password Component] Reset Password Request',
  props<{payload: ResetPasswordRequestModel}>()
);
export const resetPasswordSuccess = createAction(
  '[Auth Effects Module] Reset Password Success',
  props<{payload: ResetPasswordSuccessModel}>()
);
export const resetPasswordError = createAction(
  '[Auth Effects Module] Reset Password Error',
  props<{payload: ResetPasswordErrorModel}>()
);
export const emailVerificationError = createAction(
  '[Auth Effects Service] Verify Email Error',
  props<{payload: VerificationErrorModel}>()
);
export const resendVerificationCode = createAction(
  '[Verify Code Component] resendVerificationCode'
);
export const resendVerificationCodeSuccess = createAction(
  '[Auth Effects Service] resendVerificationCodeSuccess'
);
export const resendVerificationCodeError = createAction(
  '[Auth Effects Service] resendVerificationCodeError'
);
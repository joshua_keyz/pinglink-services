import { Injectable } from "@angular/core";
import { Subject, interval, map, takeWhile } from "rxjs";

@Injectable()
export class VerificationCounterService {
  lastSentVerification: string;

  private verificationCountDown$ = new Subject<number>();

  verificationCount$ = this.verificationCountDown$.asObservable();

  constructor() {}

  initTimer() {
    interval(1000).pipe(
      // takeWhile((_) => {
      //   return 60 - this.getSecondsExpended() > 0;
      // }),
      map(_ => {
        const seconds = this.getSecondsExpended();
        this.verificationCountDown$.next(60 - this.getSecondsExpended())
      })
    ).subscribe()
  }
  getSecondsExpended() {
    const lastSentMilliseconds = +(localStorage.getItem('lastVerificationSent') as string);
    const currentMilliseconds = Date.now();
    const timeExpended = currentMilliseconds - lastSentMilliseconds;
    const timeInSeconds= timeExpended / 1000
    return Math.ceil(timeInSeconds);
  }

}
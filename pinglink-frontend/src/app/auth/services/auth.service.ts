import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SignUpRequestModel } from "../models/signup-request.model";
import { SignUpSuccessModel } from "../models/signup-success.model";
import { VerificationSuccessModel } from "../models/verification-success.model";
import { VerificationRequestModel } from "../models/verification-request.model";
import { SignInRequestModel } from "../models/signin-request.model";
import { SignInSuccessModel } from "../models/signin-success.model";
import { ForgotPasswordRequestModel } from "../models/forgot-password-request.model";
import { ForgotPasswordResponseModel } from "../models/forgot-password-response.model";
import { ResetPasswordSuccessModel } from "../models/reset-password-success.model";

@Injectable()
export class AuthService {
  constructor(private httpClient: HttpClient){}

  registerUser(userData: SignUpRequestModel) {
    return this.httpClient.post<SignUpSuccessModel>(
      `/api/auth/users/signup`,
      userData
    );
  }

  signInUser(userData: SignInRequestModel) {
    return this.httpClient.post<SignInSuccessModel>(
      `/api/auth/users/signin`,
      userData
    );
  }

  verifyEmail(tokenData: VerificationRequestModel) {
    return this.httpClient.post<VerificationSuccessModel>(
      `/api/auth/users/verify-email`,
      tokenData
    );
  }

  verifyCode(code: string) {
    return this.httpClient.post<{success: boolean; error?: string}>(`/api/auth/users/verify-reset-password-token`, {resetToken: code})
  }

  requestResetPassword(userData: ForgotPasswordRequestModel) {
    return this.httpClient.post<ForgotPasswordResponseModel>(
      `/api/auth/users/forgot-password`,
      userData
    );
  }
  resetPassword(password: string, resetToken: string) {
    return this.httpClient.put<ResetPasswordSuccessModel>(`/api/auth/users/reset-password`, {
      resetToken, password
    });
  }
  resendVerificationCode() {
    return this.httpClient.post(`/api/auth/users/resend-verification-code`, null)
  }
  
}
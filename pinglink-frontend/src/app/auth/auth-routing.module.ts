import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SignupComponent } from "./components/signup/signup.component";
import { AuthHomeComponent } from "./components/auth-home/auth-home.component";
import { SigninComponent } from "./components/signin/signin.component";
import { ForgotPasswordComponent } from "./components/forgot-password/forgot-password.component";
import { VerifyCodeComponent } from "./components/verify-code/verify-code.component";
import { ResetPasswordComponent } from "./components/reset-password/reset-password.component";
import { EmailVerificationComponent } from "./components/email-verification/email-verification.component";
import { NotAuthGuard } from "../services/not-auth-guard.service";
import { AuthGuard } from "../services/auth-guard.service";
import { NotVerifiedGuard } from "../services/not-verified-guard.service";

const authRoutes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    component: AuthHomeComponent,
    children: [
      {
        path: 'signup',
        component: SignupComponent,
        // canActivate: [NotAuthGuard]
      },
      {
        path: 'signin',
        component: SigninComponent,
        // canActivate: [NotAuthGuard]
      },
      {
        path: 'verify-email',
        component: EmailVerificationComponent,
        canActivate: [NotVerifiedGuard]
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        canActivate: [NotAuthGuard]
      },
      {
        path: 'reset-password',
        component: ResetPasswordComponent,
        canActivate: [NotAuthGuard]
      },
      {
        path: 'verify-code',
        component: VerifyCodeComponent,
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'signup'
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(authRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AuthRoutingModule {}
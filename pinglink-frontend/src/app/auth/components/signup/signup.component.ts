import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromAuthActions from '../../state/actions';
import { SignUpRequestModel } from '../../models/signup-request.model';
import { EMPTY, Observable, of, withLatestFrom } from 'rxjs';
import { selectSignUpError } from '../../state/selectors';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private store: Store<any>) { }
  authError$: Observable<string> = EMPTY;

  signupForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  })

  ngOnInit(): void {
    this.authError$ = this.store.select(selectSignUpError);
    this.signupForm.valueChanges.pipe(
      withLatestFrom(this.authError$)
    ).subscribe((data) => {
      if(data[1]) {
        this.removeAlert();
      }
    })
  }
  signUpUser() {
    this.store.dispatch(fromAuthActions.signUpRequest({payload: this.signupForm.value as SignUpRequestModel }))
  }
  removeAlert() {
    this.store.dispatch(fromAuthActions.clearErrors())
  }
}

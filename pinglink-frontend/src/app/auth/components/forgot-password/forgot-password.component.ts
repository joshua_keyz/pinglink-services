import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { EMPTY, Observable, withLatestFrom } from 'rxjs';
import { clearErrors, sendForgotPWDRequest } from '../../state/actions';
import { ForgotPasswordRequestModel } from '../../models/forgot-password-request.model';
import { selectForgotPasswordError } from '../../state/selectors';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  constructor(private store: Store<any>) { }
  authError$: Observable<string> = EMPTY;
  forgotPasswordForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
  })

  ngOnInit(): void {
    this.authError$ = this.store.select(selectForgotPasswordError);
    this.forgotPasswordForm.valueChanges.pipe(
      withLatestFrom(this.authError$)
    ).subscribe((data) => {
      if(data[1]) {
        this.removeAlert();
      }
    })
  }
  requestPasswordReset() {
    this.store.dispatch(sendForgotPWDRequest({payload: this.forgotPasswordForm.value as ForgotPasswordRequestModel }))
  }
  removeAlert() {
    this.store.dispatch(clearErrors())
  }
}

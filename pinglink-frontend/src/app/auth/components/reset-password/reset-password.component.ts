import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { resetPasswordRequest } from '../../state/actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  constructor(
    private store: Store<any>,
    private activatedRoute: ActivatedRoute
  ) {}
  
  resetPasswordForm = new FormGroup({
    newPassword: new FormControl('', [Validators.required]),
    confirmPassword: new FormControl('', [
      Validators.required,
    ]),
  });

  ngOnInit(): void {
    this.resetPasswordForm.get('confirmPassword')?.addValidators([
      this.validateMatch.bind(this),
    ])
  }
  validateMatch(control: AbstractControl) {
    if (control.value !== this.resetPasswordForm.value.newPassword) {
      return {
        notMatchingPasswords: true,
      };
    }
    return null;
  }
  resetPassword() {
    this.store.dispatch(
      resetPasswordRequest({
        payload: {
          password: this.resetPasswordForm.value.confirmPassword as string,
          resetToken: this.activatedRoute.snapshot.queryParams['resetToken'],
        },
      })
    );
  }
  isRequiredError() {
    const formControl = this.resetPasswordForm.controls.confirmPassword;
    if(formControl.errors && formControl.errors['required']) {
      return true;
    }
    return false;
  }
  doesntMatchError() {
    const formControl = this.resetPasswordForm.controls.confirmPassword;
    if(formControl.value && formControl.errors && formControl.errors['notMatchingPasswords']) {
      return true;
    }
    return false;
  }
}

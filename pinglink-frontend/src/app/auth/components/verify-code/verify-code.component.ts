import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { EMPTY, Observable, withLatestFrom } from 'rxjs';
import { clearErrors, resendVerificationCode, verifyCodeRequest } from '../../state/actions';
import { verifyCodeError } from '../../state/selectors';

@Component({
  selector: 'app-verify-code',
  templateUrl: './verify-code.component.html',
  styleUrls: ['./verify-code.component.scss']
})
export class VerifyCodeComponent implements OnInit {
  authError$: Observable<string | undefined> = EMPTY;
  constructor(private store: Store<any>) { }
  verifyCodeForm = new FormGroup({
    verificationCode: new FormControl('', [Validators.required]),
  });
  ngOnInit(): void {
    this.authError$ = this.store.select(verifyCodeError);
    this.verifyCodeForm.valueChanges.pipe(
      withLatestFrom(this.authError$)
    ).subscribe((data) => {
      if(data[1]) {
        this.removeAlert();
      }
    })
  }
  verifyCode() {
    // this.store.dispatch(sendForgotPWDRequest({payload: this.forgotPasswordForm.value as ForgotPasswordRequestModel }))
    this.store.dispatch(verifyCodeRequest({payload: {
      verificationCode: this.verifyCodeForm.value.verificationCode as string
    }}))
  }
  removeAlert() {
    this.store.dispatch(clearErrors())
  }
  resendVerificationCode() {
    this.store.dispatch(resendVerificationCode())
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromAuthActions from '../../state/actions';
import { EMPTY, Observable, Subject, filter, takeUntil, tap, withLatestFrom } from 'rxjs';
import { selectAuthError } from '../../state/selectors';
import { VerificationCounterService } from '../../services/verification-counter.service';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.scss']
})
export class EmailVerificationComponent implements OnInit, OnDestroy {
  authError$: Observable<string> = EMPTY;
  secondsCountDown: number;
  loadingSecondsCountDown = true;
  onDestroy$ = new Subject<void>();

  constructor(private store: Store<any>, public verificationCountService: VerificationCounterService) { }
  
  verifyEmailForm = new FormGroup({
    verificationToken: new FormControl('', [Validators.required]),
  });

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
  ngOnInit(): void {
    this.authError$ = this.store.select(selectAuthError);
    this.verificationCountService.initTimer();
    this.loadingSecondsCountDown = true;
    this.verificationCountService.verificationCount$
    .pipe(
      tap(count => {
        this.loadingSecondsCountDown = false;
      }),
      takeUntil(this.onDestroy$),
      filter((count) => count >= 0)
    )
    .subscribe(
      count => {

        this.secondsCountDown = count;
        if(count === 1) {
          this.secondsCountDown = 0;
        }
      }
    )
    this.verifyEmailForm.valueChanges.pipe(
      withLatestFrom(this.authError$)
    ).subscribe((data) => {
      if(data[1]) {
        this.removeAlert();
      }
    })
  }

  verifyEmail() {
    this.store.dispatch(fromAuthActions.emailVerificationRequest({payload: this.verifyEmailForm.value as any}));
  }

  removeAlert() {
    this.store.dispatch(fromAuthActions.clearErrors());
  }
  resendVerificationCode() {
    this.store.dispatch(fromAuthActions.resendVerificationCode())
  }
}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { EMPTY, Observable, withLatestFrom } from 'rxjs';
import { selectSignInError } from '../../state/selectors';
import * as fromAuthActions from '../../state/actions';
import { SignInRequestModel } from '../../models/signin-request.model';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor(private store: Store<any>) { }
  authError$: Observable<string> = EMPTY;

  signinForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  })
  ngOnInit(): void {
    this.authError$ = this.store.select(selectSignInError);
    this.signinForm.valueChanges.pipe(
      withLatestFrom(this.authError$)
    ).subscribe((data) => {
      if(data[1]) {
        this.removeAlert();
      }
    })
  }
  signInUser() {
    this.store.dispatch(fromAuthActions.signInRequest({payload: this.signinForm.value as SignInRequestModel }))
  }
  removeAlert() {
    this.store.dispatch(fromAuthActions.clearErrors())
  }

}

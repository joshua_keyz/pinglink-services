import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignupComponent } from './components/signup/signup.component';
import { EmailVerificationComponent } from './components/email-verification/email-verification.component';
import { SigninComponent } from './components/signin/signin.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { VerifyCodeComponent } from './components/verify-code/verify-code.component';
import { AuthRoutingModule } from './auth-routing.module';
import { StoreModule } from '@ngrx/store';
import { exampleReducer } from './state/reducer';
import { AuthHomeComponent } from './components/auth-home/auth-home.component';
import { PingUiModule } from '@ping-ui/ping-ui';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './state/effects';
import { HttpClientModule } from '@angular/common/http';
import { VerificationCounterService } from './services/verification-counter.service';

@NgModule({
  declarations: [
    SignupComponent,
    EmailVerificationComponent,
    SigninComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    VerifyCodeComponent,
    AuthHomeComponent
  ],
  imports: [
    CommonModule,
    PingUiModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    HttpClientModule,
    StoreModule.forFeature('auth', exampleReducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
  providers: [
    VerificationCounterService
  ]
})
export class AuthModule { }

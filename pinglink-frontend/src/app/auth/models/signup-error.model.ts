export interface SignUpErrorModel {
  success: boolean;
  error: string;
}
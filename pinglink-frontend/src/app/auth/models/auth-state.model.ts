export interface AuthStateModel {
  token: string;
  signUpError: string;
  signInError: string;
  verifyCodeError?: string;
  forgotPasswordError: string;
}
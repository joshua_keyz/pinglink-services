export interface ResetPasswordRequestModel {
  password: string;
  resetToken: string;
}
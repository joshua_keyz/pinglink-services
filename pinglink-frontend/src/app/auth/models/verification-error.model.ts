export interface VerificationErrorModel {
  success: boolean;
  error: string;
}
export interface VerificationSuccessModel {
  success: boolean;
  token: string;
}
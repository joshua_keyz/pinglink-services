export interface ForgotPasswordErrorModel {
  success: boolean;
  error: string;
};
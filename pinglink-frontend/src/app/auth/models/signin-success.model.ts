export interface SignInSuccessModel {
  success: boolean;
  token: string;
}
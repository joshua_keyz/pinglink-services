export interface SignUpRequestModel {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}
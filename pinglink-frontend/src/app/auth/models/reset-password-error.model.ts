export interface ResetPasswordErrorModel {
  error: string;
  success: boolean;
}
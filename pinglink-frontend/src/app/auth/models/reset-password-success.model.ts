import { UserModel } from "src/app/main/models/user.model";
import { SignUpSuccessModel } from "./signup-success.model";

export interface ResetPasswordSuccessModel extends SignUpSuccessModel {
  user: UserModel
}
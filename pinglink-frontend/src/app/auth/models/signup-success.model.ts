export interface SignUpSuccessModel {
  success: boolean;
  token: string;
}
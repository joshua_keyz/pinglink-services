export interface SignInErrorModel {
  success: boolean;
  error: string;
}
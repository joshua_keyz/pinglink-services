import { FullUserModel } from "./full-user.model";

export interface ForgotPasswordResponseModel {
  success: boolean;
  data: FullUserModel;
}
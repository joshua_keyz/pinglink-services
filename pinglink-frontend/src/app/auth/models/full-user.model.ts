export interface FullUserModel {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  verified: boolean;
  createAt: string;
  __v: number;
  resetPasswordExpire: string;
  resetPasswordToken: string;
}
export interface VerificationRequestModel {
  verificationToken: string;
}
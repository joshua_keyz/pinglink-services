import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MainHomeComponent } from "./components/main-home/main-home.component";
import { ChecksComponent } from "./components/checks/checks.component";
import { NotificationsComponent } from "./components/notifications/notifications.component";
import { CheckDetailsComponent } from "./components/check-details/check-details.component";
import { CheckSettingsComponent } from "./components/check-settings/check-settings.component";
import { CheckStatsComponent } from "./components/check-stats/check-stats.component";
import { AccountHomeComponent } from "./components/account-home/account-home.component";
import { SubscriptionComponent } from "./components/subscription/subscription.component";
import { ProfileSettingsComponent } from "./components/profile-settings/profile-settings.component";
import { SecuritySettingsComponent } from "./components/security-settings/security-settings.component";
import { OrderConfirmationComponent } from "./components/order-confirmation/order-confirmation.component";
import { OrderGuardService } from "./services/order-guard.service";

const mainRoutes: Routes = [
  {
    path: '',
    component: MainHomeComponent,
    children: [
      {
        path: 'checks',
        component: ChecksComponent,
      },
      {
        path: 'account',
        component: AccountHomeComponent,
        children: [
          {
            path: 'profile-settings',
            component: ProfileSettingsComponent
          },
          {
            path: 'account-security',
            component: SecuritySettingsComponent
          },
          {
            path: 'order-confirmation',
            component: OrderConfirmationComponent,
            canActivate: [OrderGuardService]
          },
          {
            path: 'subscription',
            component: SubscriptionComponent,
          }, {
            path: '',
            pathMatch: 'full',
            redirectTo: 'profile-settings'
          }
        ]
      },
      {
        path: 'checks/:id',
        component: CheckDetailsComponent,
        children: [
          {
            path: 'settings',
            component: CheckSettingsComponent
          },
          {
            path: 'stats',
            component: CheckStatsComponent,
          }, {
            path: '',
            pathMatch: 'full',
            redirectTo: 'settings'
          }
        ]
      },
      {
        path: 'notifications',
        component: NotificationsComponent,
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'checks',
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [RouterModule]
})
export class MainRoutingModule {}
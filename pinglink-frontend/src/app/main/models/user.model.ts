import { UserSubscription } from "./subscription.model";

export interface UserModel {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  verified: boolean;
  createdAt: Date;
  profilePicture: string;
  subscription: UserSubscription
  __v: number;
}
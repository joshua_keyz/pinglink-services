export interface UserProfileDetailModel {
  firstName: string;
  lastName: string;
}
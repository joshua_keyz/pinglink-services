export interface PaymentSignatureRequest {
  amount: string;
  order_id: string;
  description: string;
  info: string;
}

export interface UserRequestErrorModel {
  success: boolean;
  error: string;
}
export interface ChangeEmailSuccessModel {
  success: boolean;
  newEmail: string;
  token: string;
}
import { UserModel } from "./user.model";

export interface UserRequestSuccessModel {
  success: boolean;
  user: UserModel
}
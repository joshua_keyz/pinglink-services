export interface Notification {
  _id: string;
  checkId: string;
  checkUrl: string;
  userId: string;
  opened: boolean;
  checkState: string;
  notificationTime: string;
}

export interface CheckRequestModel {
  protocol: string;
  url: string;
  method:string;
  successCodes: string[];
  timeoutSeconds: number;
}
export interface UploadSuccessModel {
  success: boolean;
  url: string;
}
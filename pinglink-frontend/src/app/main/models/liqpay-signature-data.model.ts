export interface LiqpaySignatureDataModel {
  signature: string;
  sign_string: string;
  data: string;
}
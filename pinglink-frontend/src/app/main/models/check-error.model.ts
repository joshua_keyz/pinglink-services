export interface CheckErrorModel {
  error: string;
  success: boolean;
}
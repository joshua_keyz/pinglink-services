import { CheckRequestModel } from "./check-request.model";

export interface CheckResponseModel {
  success: boolean;
  checks: CheckModel[]
}

export interface CheckModel extends CheckRequestModel{
  _id: string;
  userId: string;
  __v: number;
  state: 'up' | 'down';
}
export interface AllChecksResponseModel {
  success: boolean;
  length: number,
  checks: CheckModel[]
}
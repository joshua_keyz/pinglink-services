import { CheckModel, CheckResponseModel } from "./check-response.model";
import { LiqpaySignatureDataModel } from "./liqpay-signature-data.model";
import { Notification } from "./notofication.model";
import { UserModel } from "./user.model";

export interface MainStateModel {
  loading?: boolean;
  token?: string;
  user?: UserModel;
  checks: CheckModel[];
  notifications: Notification[];
  checkModalOpened?: boolean;
  deleteCheckModalOpened?: boolean;
  photoEditModalOpened?: boolean;
  deleteAccountModalOpened?: boolean;
  editEmailModalOpened?: boolean;
  editPasswordModalOpened?: boolean;
  emailChangeSteps?: number;
  securityError?: string;
  paymentSignatures?: {
    professional: LiqpaySignatureDataModel;
    enterprise: LiqpaySignatureDataModel;
  }
  confirmingOrder?: boolean;
  confirmingOrderError?: string;
}
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MainRoutingModule } from "./main-routing.module";
import { MainHomeComponent } from './components/main-home/main-home.component';
import { PingUiModule } from "@ping-ui/ping-ui";
import { ChecksComponent } from './components/checks/checks.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { StoreModule } from "@ngrx/store";
import { mainReducer } from "./state/reducer";
import { EffectsModule } from "@ngrx/effects";
import { MainEffects } from "./state/effects";
import { UserService } from "./services/user.service";
import { BannerCardComponent } from './components/banner-card/banner-card.component';
import { CheckOverviewItemComponent } from './components/check-overview-item/check-overview-item.component';
import { EnlogateNumberPipe } from './pipes/enlogate-number.pipe';
import { AppModalComponent } from "./components/modal/modal.component";
import { ReactiveFormsModule } from "@angular/forms";
import { ChecksService } from "./services/checks.service";
import { CheckDetailsComponent } from './components/check-details/check-details.component';
import { CheckSettingsComponent } from './components/check-settings/check-settings.component';
import { CheckStatsComponent } from './components/check-stats/check-stats.component';
import { AccountHomeComponent } from './components/account-home/account-home.component';
import { WSService } from "./services/ws-service";
import { PushNotificationsService } from "./services/push-notifications.service";
import { SubscriptionComponent } from './components/subscription/subscription.component';
import { ProfilePicComponent } from './components/profile-pic/profile-pic.component';
import { FileUploadService } from "./services/file-upload.service";
import { PUIImageCropperComponent } from "./components/img-cropper/img-cropper.component";
import { ProfileSettingsComponent } from "./components/profile-settings/profile-settings.component";
import { SecuritySettingsComponent } from './components/security-settings/security-settings.component';
import { UpdateEmailComponent } from "./components/update-email/update-email.component";
import { UpdatePasswordComponent } from "./components/update-password/update-password.component";
import { OrderConfirmationComponent } from "./components/order-confirmation/order-confirmation.component";
import { OrderGuardService } from "./services/order-guard.service";

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    PingUiModule,
    StoreModule.forFeature('main', mainReducer),
    EffectsModule.forFeature([MainEffects]),
    ReactiveFormsModule,
  ],
  declarations: [
    MainHomeComponent,
    ChecksComponent,
    NotificationsComponent,
    BannerCardComponent,
    CheckOverviewItemComponent,
    EnlogateNumberPipe,
    AppModalComponent,
    CheckDetailsComponent,
    CheckSettingsComponent,
    CheckStatsComponent,
    AccountHomeComponent,
    ProfileSettingsComponent,
    SubscriptionComponent,
    ProfilePicComponent,
    PUIImageCropperComponent,
    SecuritySettingsComponent,
    UpdateEmailComponent,
    UpdatePasswordComponent,
    OrderConfirmationComponent,
  ],
  providers: [
    UserService,
    ChecksService,
    WSService,
    PushNotificationsService,
    OrderGuardService,
    FileUploadService,
  ]
})
export class MainModule {}
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CheckRequestModel } from "../models/check-request.model";
import { AllChecksResponseModel, CheckModel, CheckResponseModel } from "../models/check-response.model";

@Injectable()
export class ChecksService {
  constructor(private httpClient: HttpClient) {}

  createCheck(payload: CheckRequestModel) {
    return this.httpClient.post<{success: boolean, check: CheckModel}>(
      `/api/checks/`,
      payload
    );
  }
  deleteCheck(payload: string) {
    return this.httpClient.delete<{success: boolean}>(
      `/api/checks/${payload}`
    );
  }
  updateCheck(checkId: string, payload: CheckRequestModel) {
    return this.httpClient.put<{success: boolean}>(
      `/api/checks/${checkId}`,
      payload
    );
  }

  fetchChecks() {
    return this.httpClient.get<AllChecksResponseModel>(
      `/api/checks`
    )
  }
}
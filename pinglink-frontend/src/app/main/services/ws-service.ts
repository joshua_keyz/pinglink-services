import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class WSService {
  private wsOpenSource = new Subject<any>();
  private wsMessageSource = new Subject<any>();
  private wsClosedSource = new Subject<any>();

  public wsOpen = this.wsOpenSource.asObservable();
  public wsClosed = this.wsClosedSource.asObservable();
  public wsMessage = this.wsMessageSource.asObservable();
  ws: WebSocket;

  init() {
    this.ws = new WebSocket(environment.wsHost);
    this.ws.addEventListener('open', (event) => {
      this.wsOpenSource.next(event);
    });
    this.ws.addEventListener('close', (event) => {
      this.wsClosedSource.next(event);
    });
    this.ws.addEventListener('message', (data) => {
      console.log('Message was received', data)
      this.wsMessageSource.next(data);
    });
  }

  sendMessage(message: any) {
    this.ws.send(JSON.stringify(message));
  }

}
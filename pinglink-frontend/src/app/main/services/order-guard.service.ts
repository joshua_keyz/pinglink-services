import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { OrderService } from "src/app/services/order.service";

@Injectable()
export class OrderGuardService implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if(this.orderService.getPaymentCookie()) {
      return true;
    } else {
      return this.router.navigate(['/home'])
    }
  }
  constructor(private orderService: OrderService, private router: Router){}
}
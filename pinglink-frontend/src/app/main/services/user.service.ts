import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UserRequestSuccessModel } from "../models/user-request-success.model";
import { UserProfileDetailModel } from "../models/user-profile-detail.model";
import { ChangeEmailSuccessModel } from "../models/change-email-success.model";

@Injectable()
export class UserService {
  constructor(private httpClient: HttpClient) {}

  getUserData() {
    return this.httpClient.get<UserRequestSuccessModel>(
      `/api/auth/users/current-user`
    );
  }
  updateUserProfileData(userDetails: UserProfileDetailModel) {
    return this.httpClient.post<UserRequestSuccessModel>(`/api/auth/users/update-profile-details`,
      userDetails
    );
  }
  verifyAccess(password: string) {
    return this.httpClient.post('/api/auth/users/verify-password', {
      password
    });
  }
  setNewEmail(email: string) {
    return this.httpClient.post('/api/auth/users/set-new-email', {
      email
    });
  }
  verifyNewEmail(verificationToken: string) {
    return this.httpClient.post<ChangeEmailSuccessModel>('/api/auth/users/verify-new-email', {
      verificationToken
    });
  }
  changePassword(oldPassword: string, newPassword: string) {
    return this.httpClient.post<ChangeEmailSuccessModel>('/api/auth/users/change-password', {
      oldPassword, newPassword
    });
  }
  deleteUser() {
    return this.httpClient.delete('/api/auth/users/delete-user');
  }
}
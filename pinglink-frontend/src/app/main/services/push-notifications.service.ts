import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class PushNotificationsService {
  constructor(private httpClient: HttpClient) {}
  addPushSubscriber(subscription: PushSubscription) {
    return this.httpClient.post('/api/push-notifications', {subscription});
  }
}
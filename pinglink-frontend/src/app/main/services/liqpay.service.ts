import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LiqpaySignatureDataModel } from '../models/liqpay-signature-data.model';
import { CookieService } from 'ngx-cookie-service';
import { UserSubscription } from '../models/subscription.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LiqpayService {
  getSignatureAndData(payload: {
    amount: string,
    order_id: string,
    description: string,
    info: string
  }): Observable<LiqpaySignatureDataModel> {
    return this.httpClient.get<LiqpaySignatureDataModel>(
      '/api/payments/payment-signature',
      {
        params: new HttpParams()
          .set('amount', payload.amount)
          .set('order_id', payload.order_id)
          .set('description', payload.description)
          .set('info', payload.info)
          .set(
            'result_url',
            `${environment.host}/home/account/subscription/?order_id=${payload.order_id}&info=${payload.info}`
          )
          .set('server_url',`${environment.host}/api/payments/fulfil-payment?order_id=${payload.order_id}&info=${payload.info}`),
      }
    );
  }

  setPaymentCookie(orderId: string) {
    this.cookieService.set('orderId', orderId, 100, '/home')
  }
  removePaymentCookie() {
    this.cookieService.delete('orderId', '/home');
  }
  confirmPayment(order_id: string) {
    return this.httpClient.post<{subscription: UserSubscription}>(
      '/api/payments/fulfil-payment',
      {order_id}
    );
  }
  constructor(private httpClient: HttpClient, private cookieService: CookieService) {}
}

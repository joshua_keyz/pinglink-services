import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { addNotifications, changePasswordFailure, changePasswordRequest, changePasswordSuccess, closeEditEmailModal, closePhotoEditModal, confirmPaymentFailure, confirmPaymentRequest, confirmPaymentSuccess, createCheckError, createCheckRequest, createCheckSuccess, deleteAccountFailure, deleteAccountRequest, deleteAccountSuccess, deleteCheckError, deleteCheckRequest, deleteCheckSuccess, fetchAllChecksRequest, fetchAllChecksSuccess, getEntreprisePaymentSignatureFailure, getEntreprisePaymentSignatureRequest, getEntreprisePaymentSignatureSuccess, getProfessionalPaymentSignatureFailure, getProfessionalPaymentSignatureRequest, getProfessionalPaymentSignatureSuccess, getUserDataFailure, getUserDataRequest, getUserDataSuccess, logoutUser, setNewEmailFailure, setNewEmailRequest, setNewEmailSuccess, submitForm, updateCheckError, updateCheckRequest, updateProfileDetailsFailure, updateProfileDetailsRequest, updateProfileDetailsSuccess, uploadPhotoFailure, uploadPhotoRequest, uploadPhotoSuccess, verifyAccessFailure, verifyAccessRequest, verifyAccessSuccess, verifyNewEmailFailure, verifyNewEmailRequest, verifyNewEmailSuccess } from "./actions";
import { catchError, map, of, switchMap, tap } from "rxjs";
import { Router } from "@angular/router";
import { UserService } from "../services/user.service";
import { ChecksService } from "../services/checks.service";
import { NotificationService } from "@ping-ui/ping-ui";
import { FileUploadService } from "../services/file-upload.service";
import { LiqpayService } from "../services/liqpay.service";
import { OrderService } from "src/app/services/order.service";


@Injectable()
export class MainEffects {
  logoutUser$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(logoutUser),
        tap(() => {
          localStorage.removeItem('pinglink-auth-token');
          this.router.navigateByUrl('/');
        })
      ),
    { dispatch: false }
  );

  getUserData = createEffect(() =>
    this.actions$.pipe(
      ofType(getUserDataRequest),
      switchMap((action) => this.userService.getUserData().pipe(
        map(userData => {
          return getUserDataSuccess({payload: userData})
        }),
        catchError(error => {
          return of(getUserDataFailure({payload: error}))
        })
      ))
    )
  );

  createCheck = createEffect(() =>
    this.actions$.pipe(
      ofType(createCheckRequest),
      switchMap((action) => this.checksService.createCheck(action.payload).pipe(
        map(checkResponse => {
          this.notificationService.sendNotification({
            variant: 'primary',
            content: 'Check added successfully!!!',
            timeout: 5000
          })
          return createCheckSuccess({payload: checkResponse});
        }),
        catchError(error => {
          this.notificationService.sendNotification({
            variant: 'danger',
            content: error.error.error,
            timeout: 5000,
          });
          return of(createCheckError({payload: error.error}))
        })
      ))
    )
  );
  deleteCheck = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteCheckRequest),
      switchMap((action) => this.checksService.deleteCheck(action.payload).pipe(
        switchMap(checkResponse => {
          this.notificationService.sendNotification({
            variant: 'danger',
            content: 'Check deleted successfully!!!',
            timeout: 5000
          })
          this.router.navigateByUrl('/home/checks')
          return [deleteCheckSuccess(), fetchAllChecksRequest()];
        }),
        catchError(error => {
          this.notificationService.sendNotification({
            variant: 'danger',
            content: error.error.error,
            timeout: 5000,
          });
          return of(deleteCheckError({payload: error.error}))
        })
      ))
    )
  );
  uploadPhoto = createEffect(() => this.actions$.pipe(
    ofType(uploadPhotoRequest),
    switchMap(action => this.fileUploadService.upload(action.payload.fileName, action.payload.fileUrl).pipe(
      switchMap(res => [uploadPhotoSuccess({payload: {url: res.url}}), closePhotoEditModal()]),
      catchError(err => of(uploadPhotoFailure({payload: {error: err}})))
    )),
  ));
  deleteAccount = createEffect(() => this.actions$.pipe(
    ofType(deleteAccountRequest),
    switchMap(action => this.userService.deleteUser().pipe(
      map(_ => deleteAccountSuccess()),
      catchError(error => of(deleteAccountFailure()))
    ))
  ));
  deleteAccountSuccess = createEffect(() => this.actions$.pipe(
    ofType(deleteAccountSuccess),
    tap(_ => {
      localStorage.removeItem('pinglink-auth-token')
      this.router.navigateByUrl('/');
    })
  ), {dispatch: false});
  uploadPhotoSuccess = createEffect(() => this.actions$.pipe(
    ofType(uploadPhotoSuccess),
    tap(action => this.notificationService.sendNotification({
      variant: 'primary',
      content: 'Profile picture updated successfully!!!',
      timeout: 5000
    }))
  ), {dispatch: false});
  updateProfileDetailsRequest = createEffect(() => this.actions$.pipe(
    ofType(updateProfileDetailsRequest),
    switchMap(action => this.userService.updateUserProfileData(action.payload).pipe(
      map(res => updateProfileDetailsSuccess({payload: res})),
      catchError(err => of(updateProfileDetailsFailure({payload: err})))
    ))
  ))
  getProfessionalPaymentSignature = createEffect(() => this.actions$.pipe(
    ofType(getProfessionalPaymentSignatureRequest),
    switchMap(action => this.liqPayService.getSignatureAndData(action.payload).pipe(
      map(res => getProfessionalPaymentSignatureSuccess({payload: res})),
      catchError(err => of(getProfessionalPaymentSignatureFailure({error: err})))
    ))
  ));
  getEntreprisePaymentSignature = createEffect(() => this.actions$.pipe(
    ofType(getEntreprisePaymentSignatureRequest),
    switchMap(action => this.liqPayService.getSignatureAndData(action.payload).pipe(
      map(res => getEntreprisePaymentSignatureSuccess({payload: res})),
      catchError(err => of(getEntreprisePaymentSignatureFailure({error: err})))
    ))
  ));
  updateProfileDetailsSuccess = createEffect(() => this.actions$.pipe(
    ofType(updateProfileDetailsSuccess),
    tap(() => {
      this.notificationService.sendNotification({
        variant: 'primary',
        content: 'User profile updated successfully!!!',
        timeout: 5000
      })
    }),
  ), {dispatch: false})
  verifyAccess = createEffect(() => 
    this.actions$.pipe(
      ofType(verifyAccessRequest),
      switchMap(action => this.userService.verifyAccess(action.password).pipe(
        map(res => verifyAccessSuccess()),
        catchError(err => of(verifyAccessFailure({payload: err.error})))
      ))
    )
  );
  setNewEmailRequest = createEffect(() => 
        this.actions$.pipe(
          ofType(setNewEmailRequest),
          switchMap(action => this.userService.setNewEmail(action.email).pipe(
            map((res) => setNewEmailSuccess()),
            catchError(error => of(setNewEmailFailure(error.error)))
          ))
        )
  );
  submitForm$ = createEffect(() => this.actions$.pipe(
    ofType(submitForm),
    tap(action => {
        localStorage.setItem('orderDetails', action.info);
        this.orderService.setPaymentCookie(action.orderId);
        action.orderForm.submit();
    })
), {dispatch: false})
  verifyNewEmailRequest = createEffect(() => 
        this.actions$.pipe(
          ofType(verifyNewEmailRequest),
          switchMap(action => this.userService.verifyNewEmail(action.verificationToken).pipe(
            map((res) => verifyNewEmailSuccess({payload: res})),
            tap((res) => {
              localStorage.setItem('pinglink-auth-token', res.payload.token);
            }),
            catchError(error => of(verifyNewEmailFailure(error.error)))
          ))
        )
  );
  verifyNewEmailSuccess = createEffect(() => 
    this.actions$.pipe(
      ofType(verifyNewEmailSuccess),
      map(() => closeEditEmailModal()),
      tap(() => {
        this.notificationService.sendNotification({
          variant: 'primary',
          content: 'Email updated successfully!!!',
          timeout: 5000
        })
      }) 
    )) 
  changePasswordRequest = createEffect(() => 
    this.actions$.pipe(
      ofType(changePasswordRequest),
      switchMap(action => this.userService.changePassword(action.oldPassword, action.newPassword).pipe(
        map(res => changePasswordSuccess({payload: res})),
        catchError(error => of(changePasswordFailure({error: error.error})))
      ))
    ));
  changePasswordSuccess = createEffect(() => this.actions$.pipe(
    ofType(changePasswordSuccess),
    tap((action) => {
      localStorage.setItem('pinglink-auth-token', action.payload.token);
      this.notificationService.sendNotification({
        variant: 'primary',
        content: 'Password updated successfully!!!',
        timeout: 5000
      })
    }),
  ), {dispatch: false})
  updateCheck = createEffect(() =>
    this.actions$.pipe(
      ofType(updateCheckRequest),
      switchMap((action) => this.checksService.updateCheck(action.payload.checkId, action.payload.check).pipe(
        switchMap(checkResponse => {
          this.notificationService.sendNotification({
            variant: 'primary',
            content: 'Check updated successfully!!!',
            timeout: 5000
          })
          this.router.navigateByUrl('/home/checks')
          return [deleteCheckSuccess(), fetchAllChecksRequest()];
        }),
        catchError(error => {
          this.notificationService.sendNotification({
            variant: 'danger',
            content: error.error.error,
            timeout: 5000,
          });
          return of(updateCheckError({payload: error.error}))
        })
      ))
    )
  );
  fetchAllChecks = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchAllChecksRequest),
      switchMap((_) => this.checksService.fetchChecks().pipe(
        map(checks => {
          return fetchAllChecksSuccess({payload: checks.checks});
        }),
        catchError(error => {
          return of(createCheckError({payload: error.error}))
        })
      ))
    )
  );
  confirmPaymentRequest$ = createEffect(() => this.actions$.pipe(
    ofType(confirmPaymentRequest),
    switchMap(() => {
      const orderId = this.orderService.getPaymentCookie();
      return this.liqPayService.confirmPayment(orderId).pipe(
        map(result => confirmPaymentSuccess({subscription: result.subscription})),
        catchError(error => of(confirmPaymentFailure(error.error))),
      )
    })
  ));
  confirmPaymentResult$ = createEffect(() => this.actions$.pipe(
    ofType(confirmPaymentSuccess, confirmPaymentFailure),
    tap(() => {
      this.orderService.removePaymentCookie();
    })
  ), {dispatch: false});
  addNotifications = createEffect(() =>
    this.actions$.pipe(
      ofType(addNotifications),
      tap(() => {
        this.notificationService.sendNotification({
          variant: 'primary',
          content: 'New Notification!!!',
          timeout: 5000,
        });
      })
    ), { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private userService: UserService,
    private checksService: ChecksService,
    private fileUploadService: FileUploadService,
    private notificationService: NotificationService,
    private liqPayService: LiqpayService,
    private orderService: OrderService,
  ) {}
}
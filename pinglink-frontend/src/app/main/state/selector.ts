import { createSelector } from "@ngrx/store";
import { AppStateModel } from "src/app/models/app-state.model";
import { MainStateModel } from "../models/main-state.model";

function computeDay(day: string) {
  const date = new Date(day);
  const today = new Date();
  if(today.getDate() === date.getDate()) {
    return 'Today';
  }
  if((today.getDate() - date.getDate()) === 1) {
    return 'Yesterday';
  }

  return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
}

function computeTime(dateStr: string) {
  const date = new Date(dateStr);
  let hours = date.getHours();
  let minutes: any = date.getMinutes();
  let ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

export const selectFeature = (state: AppStateModel) => state.main;
export const selectUser = createSelector(
  selectFeature,
  (state: MainStateModel) => state.user
);
export const selectCheckModalState = createSelector(
  selectFeature,
  (state: MainStateModel) => state.checkModalOpened
);
export const selectPhotoEditModalState = createSelector(
  selectFeature,
  (state: MainStateModel) => state.photoEditModalOpened
);
export const selectDeleteAccountModalState = createSelector(
  selectFeature,
  (state: MainStateModel) => state.deleteAccountModalOpened
);
export const selectEditEmailModalState = createSelector(
  selectFeature,
  (state: MainStateModel) => state.editEmailModalOpened
);
export const selectEditPasswordModalState = createSelector(
  selectFeature,
  (state: MainStateModel) => state.editPasswordModalOpened
);
export const selectSubscription = createSelector(
  selectFeature,
  (state: MainStateModel) => state.user.subscription
);
export const selectDeleteCheckModalState = createSelector(
  selectFeature,
  (state: MainStateModel) => state.deleteCheckModalOpened
);
export const selectSecurityError = createSelector(
  selectFeature,
  (state: MainStateModel) => state.securityError
);
export const selectCheckById = (id: string) => createSelector(
  selectFeature,
  (state: MainStateModel) => {
    return state.checks.find(check => check._id === id)
  }
)
export const selectChecks = createSelector(
  selectFeature,
  (state: MainStateModel) => {
    const checks = state.checks.map(check => ({
      CHECKS: check.url,
      STATUS: check.state,
      id: check._id,
      ' ': check.url
    }))
    return checks;
  }
);
export const selectNotifications = createSelector(
  selectFeature,
  (state: MainStateModel) => {
    const notifications: any[] = JSON.parse(JSON.stringify(state.notifications))
    const checks = notifications
    .sort((a, b) => {
      const date1 = new Date(a.notificationTime).valueOf();
      const date2 = new Date(b.notificationTime).valueOf();

      if(date1 > date2) {
        return -1;
      } else if(date1 < date2) {
        return 1;
      }
      return 0;
    })
    .map(notification => ({
      Ping: `${notification.checkUrl} is ${notification.checkState}`,
      Status: notification.checkState,
      Day: computeDay(notification.notificationTime),
      Time: computeTime(notification.notificationTime),
      opened: notification.opened,
      ' ': notification.checkState
    }))
    return checks;
  }
);
export const selectEmailChangeStep = createSelector(
  selectFeature,
  (state: MainStateModel) => state.emailChangeSteps
);
export const selectOrderConfirmationError = createSelector(
  selectFeature,
  (state: MainStateModel) => state.confirmingOrderError
);
export const selectOrderConfirmationProccess = createSelector(
  selectFeature,
  (state: MainStateModel) => state.confirmingOrder
);
export const selectProfessionalPaymentSignature = createSelector(
  selectFeature,
  (state: MainStateModel) => state.paymentSignatures?.professional
);
export const selectEnterprisePaymentSignature = createSelector(
  selectFeature,
  (state: MainStateModel) => state.paymentSignatures?.enterprise
);
export const selectUpChecks = createSelector(
  selectFeature,
  (state: MainStateModel) => {
    const checks = state.checks.filter(check => {
      return check.state === 'up'
    }).map(check => ({
      CHECKS: check.url,
      STATUS: check.state,
      '': check.url
    }))
    return checks;
  }
);
export const selectDownChecks = createSelector(
  selectFeature,
  (state: MainStateModel) => {
    const checks = state.checks.filter(check => {
      return check.state === 'down'
    }).map(check => ({
      CHECKS: check.url,
      STATUS: check.state,
      '': check.url
    }))
    return checks;
  }
);

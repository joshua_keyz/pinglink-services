import { createAction, props } from "@ngrx/store";
import { UserRequestSuccessModel } from "../models/user-request-success.model";
import { UserRequestErrorModel } from "../models/user-request-error.model";
import { CheckRequestModel } from "../models/check-request.model";
import { CheckModel, CheckResponseModel } from "../models/check-response.model";
import { CheckErrorModel } from "../models/check-error.model";
import { Notification } from "../models/notofication.model";
import { ChangeEmailSuccessModel } from "../models/change-email-success.model";
import { PaymentSignatureRequest } from "../models/payment-signature-request.model";
import { LiqpaySignatureDataModel } from "../models/liqpay-signature-data.model";
import { UserSubscription } from "../models/subscription.model";

export const logoutUser = createAction(
  '[Main Component] Logout Request'
);
export const getUserDataRequest = createAction(
  `[Main Component] Get the user's data request`
);
export const getUserDataSuccess = createAction(
  `[Main Effects] Retrieved the User Data Successfully`,
  props<{payload: UserRequestSuccessModel}>()
);
export const getUserDataFailure = createAction(
  `[Main Effects] Unsuccessfully retrieved the User Data`,
  props<{payload: UserRequestErrorModel}>()
);
export const createCheckRequest = createAction(
  `[Checks Component] Create Check Request`,
  props<{payload: CheckRequestModel}>()
);
export const openPhotoEditModal = createAction(
  `[Account Settings Component] Open Photo Edit Modal`,
);
export const closePhotoEditModal = createAction(
  `[Main Effects] Close Photo Edit Modal`,
);
export const openEditEmailModal = createAction(
  `[Account Settings Component] Open Edit Email Modal`,
);
export const closeEditEmailModal = createAction(
  `[Main Effects] Close Edit Email Modal`,
);
export const openEditPasswordModal = createAction(
  `[Account Settings Component] Open Edit Password Modal`,
);
export const closeEditPasswordModal = createAction(
  `[Main Effects] Close Edit Password Modal`,
);
export const openDeleteAccountModal = createAction(
  '[Account Security Component] Open Delete Account Modal'
);
export const closeDeleteAccountModal = createAction(
  '[Main Effects] Close Delete Account Modal'
);
export const updateProfileDetailsRequest = createAction(
  '[Account Settings Component] Update Profile Details Request',
  props<{payload: {firstName: string; lastName: string}}>(),
);
export const updateProfileDetailsSuccess = createAction(
  '[Main Effects] Update Profile Details Success',
  props<{payload: UserRequestSuccessModel}>(),
);
export const updateProfileDetailsFailure = createAction(
  '[Main Effects] Update Profile Details Failure',
  props<{payload: UserRequestErrorModel}>(),
);
export const uploadPhotoRequest = createAction(
  '[Account Settings Component] Upload Photo Request',
  props<{payload: {fileName: string; fileUrl: string}}>(),
);
export const uploadPhotoSuccess = createAction(
  '[Main Effects] Upload Photo Success',
  props<{payload: {url: string}}>(),
);
export const uploadPhotoFailure = createAction(
  '[Main Effects] Upload Photo Failure',
  props<{payload: {error: string}}>(),
);
export const updateCheckLocally = createAction(
  '[Main Home Component] Update Check Locally',
  props<{payload: {checkId: string; state: 'up'| 'down'}}>(),
);
export const createCheckSuccess = createAction(
  `[Main Effects] Create Check Success`,
  props<{payload: {success: boolean, check: CheckModel}}>()
);
export const createCheckError = createAction(
  `[Main Effects] Create Check Error`,
  props<{payload: CheckErrorModel}>()
);
export const deleteCheckRequest = createAction(
  `[Checks Component] Delete Check Request`,
  props<{payload: string}>()
);
export const deleteCheckSuccess = createAction(
  `[Main Effects] Delete Check Success`,
);
export const deleteCheckError = createAction(
  `[Main Effects] Delete Check Error`,
  props<{payload: CheckErrorModel}>()
);
export const updateCheckRequest = createAction(
  `[Checks Component] Update Check Request`,
  props<{payload: {check: CheckRequestModel, checkId: string}}>()
);
export const updateCheckSuccess = createAction(
  `[Main Effects] Update Check Success`,
  props<{success: boolean, check: CheckModel}>()
);
export const updateCheckError = createAction(
  `[Main Effects] Update Check Error`,
  props<{payload: CheckErrorModel}>()
);
export const openAddCheckModal = createAction(
  `[Checks Component] Open Add Check Modal`
);
export const closeAddCheckModal = createAction(
  `[Checks Component] Close Add Check Modal`
);
export const closeDeleteCheckModal = createAction(
  `[Checks Effects] Close Delete Check Modal`
);
export const openDeleteCheckModal = createAction(
  `[Checks Settings Component] Open Delete Check Modal`
);
export const fetchAllChecksRequest = createAction(
  `[Checks Component] Fetch All Checks Request`,
);
export const fetchAllChecksSuccess = createAction(
  `[Main Effects] Fetch All Checks Success`,
  props<{payload: CheckModel[]}>()
);
export const fetchAllChecksError = createAction(
  `[Main Effects] Fetch All Checks Error`,
  props<{payload: CheckErrorModel}>()
);
export const loadNotifications = createAction(
  `[Main Home Components] Load Notifications`,
  props<{payload: Notification[]}>(),
);
export const addNotifications = createAction(
  `[Main Home Components] Add Notifications`,
  props<{payload: Notification}>(),
);
export const verifyAccessRequest = createAction(
  `[Security Settings Component] Verify Access Request`,
  props<{password: string}>(),
);
export const verifyAccessSuccess = createAction(
  `[Main Effects] Verify Access Success`,
);
export const verifyAccessFailure = createAction(
  `[Security Settings Component] Verify Access Failure`,
  props<{payload: CheckErrorModel}>(),
);
export const setNewEmailRequest = createAction(
  `[Security Settings Component] Set New Email Request`,
  props<{email: string}>()
);
export const setNewEmailSuccess = createAction(
  `[Main Effects] Set New email Success`
);
export const setNewEmailFailure = createAction(
  `[Main Effects] Set New email Failures`,
  props<{payload: CheckErrorModel}>(),
);
export const verifyNewEmailRequest = createAction(
  `[Security Settings Component] Verify New Email Request`,
  props<{verificationToken: string}>()
);
export const verifyNewEmailSuccess = createAction(
  `[Main Effects] Verify New email Success`,
  props<{payload: ChangeEmailSuccessModel}>(),
);
export const verifyNewEmailFailure = createAction(
  `[Main Effects] Verify New email Failure`,
  props<{payload: CheckErrorModel}>(),
);

export const changePasswordRequest = createAction(
  `[Security Settings Component] Change Password Request`,
  props<{oldPassword: string, newPassword: string}>()
);
export const changePasswordSuccess = createAction(
  `[Main Effects] Change Password Success`,
  props<{payload: ChangeEmailSuccessModel}>()
);
export const changePasswordFailure = createAction(
  `[Main Effects] Change Password Failure`,
  props<{error: string}>()
);
export const clearSecurityError = createAction(
  `[Security Settings Component] Clear Security Error`
);
export const deleteAccountRequest = createAction(
  `[Security Settings Component] Delete Account Request`
);
export const deleteAccountSuccess = createAction(
  `[Main Effects] Delete Account Success`
);
export const deleteAccountFailure = createAction(
  `[Main Effects] Delete Account Failure`
);
export const getProfessionalPaymentSignatureRequest = createAction(
  `[Subscription Component] Get Professional Payment Signature Request`,
  props<{payload: PaymentSignatureRequest}>(),
);
export const getProfessionalPaymentSignatureSuccess = createAction(
  `[Main Effects] Get Professional Payment Signature Success`,
  props<{payload: LiqpaySignatureDataModel}>(),
);
export const getProfessionalPaymentSignatureFailure = createAction(
  `[Main Effects] Get Professional Payment Signature Failure`,
  props<{error: any}>(),
);
export const getEntreprisePaymentSignatureRequest = createAction(
  `[Subscription Component] Get Entreprise Payment Signature Request`,
  props<{payload: PaymentSignatureRequest}>(),
);
export const getEntreprisePaymentSignatureSuccess = createAction(
  `[Main Effects] Get Entreprise Payment Signature Success`,
  props<{payload: LiqpaySignatureDataModel}>(),
);
export const getEntreprisePaymentSignatureFailure = createAction(
  `[Main Effects] Get Entreprise Payment Signature Failure`,
  props<{error: any}>(),
);
export const submitForm = createAction(
  `[Subscription Component] Submit Form`,
  props<{orderForm: HTMLFormElement, orderId: string; info: string}>()
)
export const confirmPaymentRequest = createAction(
  `[Order Confirmation Component] Confirm Payment Request`
);
export const confirmPaymentSuccess = createAction(
  `[Main Effects] Confirm Payment Success`,
  props<{subscription: UserSubscription}>()
);
export const confirmPaymentFailure = createAction(
  `[Main Effects] Confirm Payment Failure`,
  props<{error: string}>()
);
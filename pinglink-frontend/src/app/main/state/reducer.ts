import { createReducer, on } from "@ngrx/store";
import { addNotifications, changePasswordFailure, changePasswordSuccess, clearSecurityError, closeAddCheckModal, closeDeleteAccountModal, closeDeleteCheckModal, closeEditEmailModal, closeEditPasswordModal, closePhotoEditModal, confirmPaymentFailure, confirmPaymentRequest, confirmPaymentSuccess, createCheckError, createCheckSuccess, deleteCheckSuccess, fetchAllChecksSuccess, getEntreprisePaymentSignatureSuccess, getProfessionalPaymentSignatureSuccess, getUserDataRequest, getUserDataSuccess, loadNotifications, openAddCheckModal, openDeleteAccountModal, openDeleteCheckModal, openEditEmailModal, openEditPasswordModal, openPhotoEditModal, setNewEmailSuccess, updateCheckLocally, updateProfileDetailsRequest, updateProfileDetailsSuccess, uploadPhotoSuccess, verifyAccessSuccess, verifyNewEmailSuccess } from "./actions";
import { MainStateModel } from "../models/main-state.model";
import { CheckModel } from "../models/check-response.model";

export const initialState: MainStateModel = {
  checks: [],
  notifications: [],
   emailChangeSteps: 1
};

export const mainReducer = createReducer(
  initialState,
  on(getUserDataRequest, updateProfileDetailsRequest, (state) => ({ ...state, loading: true })),
  on(getUserDataSuccess, updateProfileDetailsSuccess, (state, action) => ({
    ...state,
    loading: false,
    user: action.payload.user,
  })),
  on(openAddCheckModal, (state) => ({ ...state, checkModalOpened: true })),
  on(openDeleteAccountModal, (state) => ({ ...state, deleteAccountModalOpened: true })),
  on(closeDeleteAccountModal, (state) => ({ ...state, deleteAccountModalOpened: false })),
  on(openDeleteCheckModal, (state) => ({ ...state, deleteCheckModalOpened: true })),
  on(createCheckSuccess, (state, action) => ({
    ...state,
    checkModalOpened: false,
    checks: state.checks.concat(action.payload.check)
  })),
  on(verifyAccessSuccess, (state) => ({
    ...state,
    emailChangeSteps: 2,
  })),
  on(changePasswordSuccess, (state, action) => ({
    ...state,
    editPasswordModalOpened: false,
  })),
  on(changePasswordFailure, (state, action) => ({
    ...state,
    securityError: action.error['error']
  })),
  on(verifyNewEmailSuccess, (state, action) => ({
    ...state,
    user: {...state.user, email: action.payload.newEmail},
    emailChangeSteps: 1
  })),
  on(confirmPaymentRequest, (state, action) => ({
    ...state,
    processingPayment: true,
  })),
  on(confirmPaymentSuccess, (state, action) => ({
    ...state,
    confirmingOrder: false,
    user: {
      ...state.user,
      subscription: action.subscription
    }
  })),
  on(confirmPaymentFailure, (state, action) => ({
    ...state,
    confirmingOrder: false,
    confirmingOrderError: action.error
  })),
  on(setNewEmailSuccess, (state) => ({
    ...state,
    emailChangeSteps: 3,
  })),
  on(getProfessionalPaymentSignatureSuccess, (state, action) => ({
    ...state,
    paymentSignatures: {
      ...state.paymentSignatures,
      professional: action.payload
    }
  })),
  on(getEntreprisePaymentSignatureSuccess, (state, action) => ({
    ...state,
    paymentSignatures: {
      ...state.paymentSignatures,
      enterprise: action.payload
    }
  })),
  on(uploadPhotoSuccess, (state, action) => ({
    ...state,
    user: {
      ...state.user!,
      profilePicture: action.payload.url
    }
  })),
  on(closeAddCheckModal, createCheckError, (state) => ({
    ...state,
    checkModalOpened: false
  })),
  on(closeDeleteCheckModal, deleteCheckSuccess, createCheckError, (state) => ({
    ...state,
    deleteCheckModalOpened: false
  })),
  on(clearSecurityError, (state, action) => ({
    ...state,
    securityError: '',
  })),
  on(fetchAllChecksSuccess, (state, action) => ({
    ...state,
    checks: action.payload,
  })),
  on(loadNotifications, (state, action) => ({
    ...state,
    notifications: action.payload
  })),
  on(addNotifications, (state, action) => ({
    ...state,
    notifications: [...state.notifications, action.payload]
  })),
  on(openPhotoEditModal, (state) => ({
    ...state,
    photoEditModalOpened: true,
  })),
  on(openEditEmailModal, (state) => ({
    ...state,
    editEmailModalOpened: true,
  })),
  on(openEditPasswordModal, (state) => ({
    ...state,
    editPasswordModalOpened: true,
  })),
  on(closeEditPasswordModal, (state) => ({
    ...state,
    editPasswordModalOpened: false,
  })),
  on(closePhotoEditModal, (state) => ({
    ...state,
    photoEditModalOpened: false,
  })),
  on(closeEditEmailModal, (state) => ({
    ...state,
    editEmailModalOpened: false,
  })),

  on(updateCheckLocally, (state, action) => {
    const checks: CheckModel[] = JSON.parse(JSON.stringify(state.checks))
    const checkToUpdateIdx = checks.findIndex(check => check._id === action.payload.checkId);
    checks[checkToUpdateIdx].state = action.payload.state;
    return {
      ...state,
      checks,
    }
  }),
);
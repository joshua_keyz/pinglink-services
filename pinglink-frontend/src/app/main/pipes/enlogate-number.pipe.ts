import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enlogateNumber',
  pure: true,
})
export class EnlogateNumberPipe implements PipeTransform {

  transform(value: number, numberOfDigits = 2): string {
    let strNumber = value.toString();
    if(strNumber.length < numberOfDigits) {
      const lengthDiff = numberOfDigits - strNumber.length;
      strNumber = strNumber.padStart(numberOfDigits, '0')
    }
    return strNumber;
  }

}

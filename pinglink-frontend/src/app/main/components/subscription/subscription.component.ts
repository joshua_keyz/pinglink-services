import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LiqpayService } from '../../services/liqpay.service';
import { Store } from '@ngrx/store';
import { getEntreprisePaymentSignatureRequest, getProfessionalPaymentSignatureRequest, getProfessionalPaymentSignatureSuccess, submitForm } from '../../state/actions';
import { selectEnterprisePaymentSignature, selectProfessionalPaymentSignature, selectSubscription } from '../../state/selector';
import { PaymentSignatureRequest } from '../../models/payment-signature-request.model';
import { v4 as uuidv4 } from 'uuid';
@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
  @ViewChild('orderForm2') orderForm2: ElementRef<HTMLFormElement>;
  @ViewChild('orderForm3') orderForm3: ElementRef<HTMLFormElement>;
  professionalId = uuidv4();
  enterpriseId = uuidv4();
  professionalPaymentSignature$ = this.store.select(selectProfessionalPaymentSignature);
  enterprisePaymentSignature$ = this.store.select(selectEnterprisePaymentSignature);
  professionalInfo: PaymentSignatureRequest = {
    amount: '10',
    description: 'Professional Subscription Package',
    order_id: this.professionalId,
    info: 'Info'
  }
  entrepriseInfo: PaymentSignatureRequest = {
    amount: '30',
    description: 'Entreprise Subscription Package',
    order_id: this.enterpriseId,
    info: 'Info'
  };

  userSubscription$ = this.store.select(selectSubscription);

  orderProfessional() {
    const action = "https://www.liqpay.ua/api/3/checkout";
    this.orderForm2.nativeElement.action = action;
    this.store.dispatch(submitForm({orderId: this.professionalId, orderForm: this.orderForm2.nativeElement, info: JSON.stringify(this.professionalInfo)}));
  }
  orderEnterprise() {
    const action = "https://www.liqpay.ua/api/3/checkout";
    this.orderForm3.nativeElement.action = action;
    this.store.dispatch(submitForm({orderId: this.enterpriseId, orderForm: this.orderForm3.nativeElement, info: JSON.stringify(this.entrepriseInfo)}));
  }
  ngOnInit(): void {
    this.store.dispatch(getProfessionalPaymentSignatureRequest({payload: this.professionalInfo}));
    this.store.dispatch(getEntreprisePaymentSignatureRequest({payload: this.entrepriseInfo}));
  }
  placeOrder() {

  }
  constructor(private store: Store) {}
}

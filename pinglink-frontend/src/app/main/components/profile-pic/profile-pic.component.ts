import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-profile-pic',
  templateUrl: './profile-pic.component.html',
  styleUrls: ['./profile-pic.component.scss'],
  animations: [
    trigger('hover', [
      state('active', style({
        opacity: 1,
      })),
      state('inactive', style({
        opacity: 0,
      })),
      transition('active <=> inactive', [
        animate(200)
      ])
    ])
  ]
})
export class ProfilePicComponent {
  isActive = false;
  @Input() profilePic: string;
  @Output() edit = new EventEmitter();

  onMouseEnter() {
    this.isActive = true;
  }
  onMouseLeave() {
    this.isActive = false;
  }
}

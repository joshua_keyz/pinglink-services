import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { MenuItemModel, NotificationService } from '@ping-ui/ping-ui';
import { addNotifications, fetchAllChecksRequest, getUserDataRequest, loadNotifications, logoutUser, updateCheckLocally } from '../../state/actions';
import { Observable, Subject, mergeMap, takeUntil, tap } from 'rxjs';
import { UserModel } from '../../models/user.model';
import { selectSubscription, selectUser } from '../../state/selector';
import { v4 as uuid } from 'uuid';
import { WSRegistryModel } from 'src/app/models/ws-registry.model';
import { WSService } from '../../services/ws-service';
import { SwPush } from '@angular/service-worker';
import { PushNotificationsService } from '../../services/push-notifications.service';


@Component({
  selector: 'app-main-home',
  templateUrl: './main-home.component.html',
  styleUrls: ['./main-home.component.scss'],
})
export class MainHomeComponent implements OnInit, OnDestroy {
  user$?: Observable<UserModel | undefined>;
  ws: WebSocket | undefined;
  private onDestroy$ = new Subject();
  sub: PushSubscription;
  userSubscription$ = this.store.select(selectSubscription);
  modalOpen = false;
  readonly VAPID_PUBLIC_KEY =
    'BJu7vLYx78JP-J83AVbs35HLe7uxAcOcZlwRHZ0efG6YToHopmr1_daxOutkUjGuhZi0kwFw2WZOeR7D8eJyGQ8';

  menuItems: MenuItemModel[] = [
    {
      icon: 'icon-web',
      item: 'Checks',
      link: '/home/checks',
    },
    {
      icon: 'icon-notification-bell',
      item: 'Notifications',
      link: '/home/notifications',
    },
  ];
  profileMenu: MenuItemModel[] = [
    {
      icon: 'icon-account-circle',
      item: 'Edit Profile',
      link: '/home/account/profile-settings',
    },
    {
      icon: 'icon-account-box',
      item: 'Account Security',
      link: '/home/account/account-security',
    },
    {
      icon: 'icon-account-circle',
      item: 'Subscription',
      link: '/home/account/subscription',
    },
  ];

  logout() {
    this.store.dispatch(logoutUser());
  }
  closeModal() {
    this.modalOpen = false;
  }
  subscribeToNotifications() {
    this.swPush
      .requestSubscription({
        serverPublicKey: this.VAPID_PUBLIC_KEY,
      })
      .then((sub) => {
        console.log(sub);
        this.sub = sub;
        this.pushNotif.addPushSubscriber(sub).subscribe(
          () => console.log('Sent push subscription object to server'),
          err => console.log('Could not send subscription object to server, reason:', err)
        );
      })
      .catch((err) =>
        console.error('Could not subscribe to notifications', err)
      );
    this.closeModal();
  }
  ngOnInit(): void {
    this.store.dispatch(getUserDataRequest());
    this.store.dispatch(fetchAllChecksRequest());
    this.user$ = this.store.select(selectUser);
    const id = uuid();
    this.wsService.init();
    console.log(this.swPush)
    this.swPush.subscription.subscribe(activeSubscription => {
      console.log(activeSubscription)
      if(!activeSubscription) {
        this.modalOpen = true;
      }
    })
    this.wsService.wsOpen
      .pipe(
        mergeMap((event) =>
          this.user$!.pipe(
            takeUntil(this.onDestroy$),
            tap((data) => {
              if (data) {
                const registry: WSRegistryModel = {
                  type: 'registry',
                  payload: {
                    id: data._id,
                    email: data.email,
                    connectionId: id,
                  },
                };
                this.wsService.sendMessage(JSON.stringify(registry));
              }
            })
          )
        )
      )
      .subscribe();
    this.wsService.wsClosed
      .pipe(
        takeUntil(this.onDestroy$),
        tap((data) => {
          // alert('WS was closed')
        })
      ).subscribe();
    this.wsService.wsMessage
      .pipe(
        takeUntil(this.onDestroy$),
        tap((data: any) => {
          const parsedData = JSON.parse(data.data);
          switch (parsedData.type) {
            case 'notification': {
              this.store.dispatch(
                loadNotifications({ payload: parsedData.payload })
              );
              break;
            }
            case 'new-notification': {
              this.store.dispatch(
                addNotifications({ payload: parsedData.payload })
              );
              this.store.dispatch(
                updateCheckLocally({
                  payload: {
                    checkId: parsedData.payload.checkId,
                    state: parsedData.payload.checkState,
                  },
                })
              );
              break;
            }
            default: {
              alert('Default message here');
            }
          }
        })
      )
      .subscribe();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.complete();
  }

  constructor(
    private store: Store<any>,
    private swPush: SwPush,
    private wsService: WSService,
    public notificationService: NotificationService,
    public pushNotif: PushNotificationsService
  ) {}
}

import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class AppModalComponent {
  @Output() close: EventEmitter<void> = new EventEmitter();
  @Input() content: any;
  @Input() width = 500;
  @Input() height = 600;
  @Input() top = 100;

  get style() {
    return {
      minWidth: `${this.width}px`,
      minHeight: `${this.height}px`,
      marginTop: `${this.top}px`,
    }
  }
}
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { closeAddCheckModal, createCheckRequest, openAddCheckModal } from '../../state/actions';
import { selectCheckModalState, selectChecks, selectDownChecks, selectUpChecks, selectUser } from '../../state/selector';

@Component({
  selector: 'app-checks',
  templateUrl: './checks.component.html',
  styleUrls: ['./checks.component.scss']
})
export class ChecksComponent implements OnInit {
  checksData$ = this.store.select(selectChecks);
  upChecks = this.store.select(selectUpChecks);
  downChecks = this.store.select(selectDownChecks);
  userData$ = this.store.select(selectUser);
  checksColumns = ['CHECKS', 'STATUS', ' ']
  modalOpen = true;
  successCodesOptions = [200, 201, 302, 304];
  methodOptions = ['GET', 'POST', 'PUT', 'DELETE'];
  protocolOptions = ['HTTP', 'HTTPS'];
  checkModalState$ = this.store.select(selectCheckModalState);

  addCheckForm = new FormGroup({
    url: new FormControl('', Validators.required),
    protocol: new FormControl(this.protocolOptions[0], Validators.required),
    method: new FormControl(this.methodOptions[0], Validators.required),
    successCodes: new FormControl(this.successCodesOptions[0], Validators.required),
    timeoutSeconds: new FormControl('', [Validators.required, Validators.max(5)]),
  });

  constructor(private store: Store<any>){}
  ngOnInit(): void {
  }
  openAddCheckModal() {
    this.store.dispatch(openAddCheckModal())
  }
  closeAddCheckModal() {
    this.store.dispatch(closeAddCheckModal())
  }
  createCheck() {
    const checkFormValue = this.addCheckForm.value as any;
    checkFormValue.protocol = checkFormValue.protocol.toLowerCase();
    checkFormValue.method = checkFormValue.method.toLowerCase();
    this.store.dispatch(createCheckRequest({payload: this.addCheckForm.value as any}))
  }
}

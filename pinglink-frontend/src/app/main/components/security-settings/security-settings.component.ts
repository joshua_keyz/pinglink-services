import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { closeDeleteAccountModal, closeEditEmailModal, closeEditPasswordModal, closePhotoEditModal, deleteAccountRequest, openDeleteAccountModal, openEditEmailModal, openEditPasswordModal, openPhotoEditModal, uploadPhotoRequest } from '../../state/actions';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { selectDeleteAccountModalState, selectEditEmailModalState, selectEditPasswordModalState, selectPhotoEditModalState, selectUser } from '../../state/selector';

@Component({
  selector: 'app-security-settings',
  templateUrl: './security-settings.component.html',
  styleUrls: ['./security-settings.component.scss']
})
export class SecuritySettingsComponent implements OnInit {
  user$?: Observable<UserModel | undefined>;
  confirmationCode = '';
  editAccountSecurityForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
  });
  deleteAccountForm = new FormGroup({
    confirmationCode: new FormControl('', [Validators.required, this.validateConfirmationCode.bind(this)]),
  });
  editProfileModalOpened$ = this.store.select(selectPhotoEditModalState);
  deleteAccountModalOpened$ = this.store.select(selectDeleteAccountModalState);
  editEmailModalOpened$ = this.store.select(selectEditEmailModalState);
  editPasswordModalOpened$ = this.store.select(selectEditPasswordModalState);
  ngOnInit(): void {
    this.user$ = this.store.select(selectUser);
  }
  editEmail() {
    this.store.dispatch(openEditEmailModal())
  }
  editPassword() {
    this.store.dispatch(openEditPasswordModal());
  }
  genRandomCode(length: number) {
    const alphabets = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase();
    let result = '';
    for(let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * alphabets.length);
      result += alphabets[randomIndex];
    }
    return result;
  }
  closeEditPasswordModal() {
    this.store.dispatch(closeEditPasswordModal());
  }
  openCloseAccount() {
    this.confirmationCode = this.genRandomCode(7);
    this.store.dispatch(openDeleteAccountModal());
  }
  closeDeleteAccountModal() {
    this.store.dispatch(closeDeleteAccountModal());
  }
  closeEditEmailModal() {
    this.store.dispatch(closeEditEmailModal());
  }
  deleteAccount() {
    this.store.dispatch(deleteAccountRequest())
  }
  onEditProfile() {
    this.store.dispatch(openPhotoEditModal())
  }
  uploadPhoto(photoData: any) {
    this.store.dispatch(uploadPhotoRequest({payload: {fileName: "fileName", fileUrl: photoData}}));
  }
  closeProfile() {
    this.store.dispatch(closePhotoEditModal())
  }
  validateConfirmationCode(control: AbstractControl) {

    if(!(control.value.toLowerCase() === this.confirmationCode.toLowerCase())) {
      return {invalidConfirmationCode: true};
    }
    return null;
  }
  doesntMatchError() {
    const formControl = this.deleteAccountForm.controls.confirmationCode;
    if(formControl.errors && formControl.errors['invalidConfirmationCode']) {
      return true;
    }
    return false;
  }
  isRequiredError() {
    const formControl = this.deleteAccountForm.controls.confirmationCode;
    if(formControl.errors && formControl.errors['required']) {
      return true;
    }
    return false;
  }
  constructor(private store: Store<any>){}

}

import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Store } from "@ngrx/store";
import { changePasswordRequest, clearSecurityError } from "../../state/actions";
import { selectSecurityError } from "../../state/selector";
import { Subject, takeUntil } from "rxjs";

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit{
  securityError$ = this.store.select(selectSecurityError);
  changePasswordForm = new FormGroup({
    oldPassword: new FormControl(''),
    newPassword: new FormControl(''),
  });
  onDestroy$ = new Subject();

  changePassword() {
    const { oldPassword, newPassword } = this.changePasswordForm.value;
    this.store.dispatch(changePasswordRequest({oldPassword, newPassword}))
  }
  removeAlert() {
    this.store.dispatch(clearSecurityError());
  }
  ngOnInit(): void {
    this.changePasswordForm.valueChanges.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(_ => this.removeAlert());
  }
  constructor(private store: Store) {}
}
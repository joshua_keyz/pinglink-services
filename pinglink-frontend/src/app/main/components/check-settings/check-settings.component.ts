import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { EMPTY, Observable, Subject, of, takeUntil } from 'rxjs';
import { selectCheckById, selectDeleteCheckModalState } from '../../state/selector';
import { CheckModel } from '../../models/check-response.model';
import { closeDeleteCheckModal, deleteCheckRequest, openDeleteCheckModal, updateCheckRequest } from '../../state/actions';

@Component({
  selector: 'app-check-settings',
  templateUrl: './check-settings.component.html',
  styleUrls: ['./check-settings.component.scss']
})
export class CheckSettingsComponent implements OnInit, OnDestroy {
  check$: Observable<any> = EMPTY;
  successCodesOptions = [200, 201, 302, 304];
  methodOptions = ['GET', 'POST', 'PUT', 'DELETE'];
  confirmationCode = '';
  protocolOptions = ['HTTP', 'HTTPS'];
  checkId = '';
  deleteCheckModalState$ = this.store.select(selectDeleteCheckModalState)
  ngOnDestroy$ = new Subject<any>();
  editCheckForm = new FormGroup({
    url: new FormControl('', Validators.required),
    protocol: new FormControl(this.protocolOptions[0], Validators.required),
    method: new FormControl(this.methodOptions[0], Validators.required),
    successCodes: new FormControl(this.successCodesOptions[0], Validators.required),
    timeoutSeconds: new FormControl('', [Validators.required, Validators.max(5)]),
  });
  deleteCheckForm = new FormGroup({
    confirmationCode: new FormControl('', [Validators.required, this.validateConfirmationCode.bind(this)]),
  });
  constructor(private activatedRoute: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.checkId = this.activatedRoute.snapshot.parent?.params['id']
    this.store.select(selectCheckById(this.checkId)).pipe(
      takeUntil(this.ngOnDestroy$)
    ).subscribe((check) => {
      const checkData = check as CheckModel;
      if(checkData) {
        this.editCheckForm.patchValue({
          url: checkData.url,
          protocol: checkData.protocol.toUpperCase(),
          method: checkData.method.toUpperCase(),
          successCodes: 200,
          timeoutSeconds: checkData.timeoutSeconds.toString()
  
        });
      }
    });
  }
  openModal() {
    this.confirmationCode = this.genRandomCode(7);
    this.store.dispatch(openDeleteCheckModal())
  }
  closeDeleteCheckModal() {
    this.store.dispatch(closeDeleteCheckModal())
    this.deleteCheckForm.setValue({
      confirmationCode: ''
    });
    this.deleteCheckForm.markAsPristine();
  }
  getRandomSevenLetters() {
    return 'XWTYFTR';
  }
  ngOnDestroy(): void {
    this.ngOnDestroy$.complete();
  }
  genRandomCode(length: number) {
    const alphabets = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase();
    let result = '';
    for(let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * alphabets.length);
      result += alphabets[randomIndex];
    }
    return result;
  }
  validateConfirmationCode(control: AbstractControl) {

    if(!(control.value.toLowerCase() === this.confirmationCode.toLowerCase())) {
      return {invalidConfirmationCode: true};
    }
    return null;
  }
  deleteCheck() {
    this.store.dispatch(deleteCheckRequest({payload: this.checkId}))
    // this.closeDeleteCheckModal();
  }
  doesntMatchError() {
    const formControl = this.deleteCheckForm.controls.confirmationCode;
    if(formControl.errors && formControl.errors['invalidConfirmationCode']) {
      return true;
    }
    return false;
  }
  isRequiredError() {
    const formControl = this.deleteCheckForm.controls.confirmationCode;
    if(formControl.errors && formControl.errors['required']) {
      return true;
    }
    return false;
  }
  updateCheck() {
    this.store.dispatch(updateCheckRequest({payload: {check: this.editCheckForm.value as any, checkId: this.checkId}}))
  }
}

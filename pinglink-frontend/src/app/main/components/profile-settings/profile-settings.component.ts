import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject, takeUntil } from 'rxjs';
import { UserModel } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { selectPhotoEditModalState, selectUser } from '../../state/selector';
import { FileUploadService } from '../../services/file-upload.service';
import { closePhotoEditModal, openPhotoEditModal, updateProfileDetailsRequest, uploadPhotoRequest } from '../../state/actions';
import { UserProfileDetailModel } from '../../models/user-profile-detail.model';

@Component({
  selector: 'app-account-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.scss']
})
export class ProfileSettingsComponent implements OnInit, OnDestroy {
  user$?: Observable<UserModel | undefined>;
  onDestroy$ = new Subject();
  editAccountForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
  });
  originalFirstName = '';
  originalLastName = '';
  userDataHasChanged = false;
  editProfileModalOpened$ = this.store.select(selectPhotoEditModalState);
  ngOnInit(): void {
    this.user$ = this.store.select(selectUser);
    this.user$.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(
      userData => {
        this.editAccountForm.setValue({
          firstName: userData.firstName,
          lastName: userData.lastName,
        });
        this.originalFirstName = userData.firstName;
        this.originalLastName = userData.lastName;
        this.userDataHasChanged = false;

      }
    )
    this.editAccountForm.valueChanges.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(changes => {
      if (this.checkProfileChanges(changes)) {
        this.userDataHasChanged = true;
      } else {
        this.userDataHasChanged = false;
      }
    })
  }
  onEditProfile() {
    this.store.dispatch(openPhotoEditModal())
  }
  checkProfileChanges(changes: Partial<{firstName: string; lastName: string}>) {
    return changes.firstName.trim() !== this.originalFirstName ||
        changes.lastName.trim() !== this.originalLastName
  }
  ngOnDestroy(): void {
    this.onDestroy$.complete();
  }
  updateProfileDetails() {
    this.store.dispatch(updateProfileDetailsRequest({payload: this.editAccountForm.value as UserProfileDetailModel}))
  }
  closeProfile() {
    this.store.dispatch(closePhotoEditModal())
  }
  uploadPhoto(photoData: any) {
    this.store.dispatch(uploadPhotoRequest({payload: {fileName: "fileName", fileUrl: photoData}}));
  }
  constructor(private store: Store<any>){}
}

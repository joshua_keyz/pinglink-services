import { Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-check-details',
  templateUrl: './check-details.component.html',
  styleUrls: ['./check-details.component.scss']
})
export class CheckDetailsComponent implements DoCheck{
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}
  active: 'settings' | 'stats' = 'settings';

  ngDoCheck(): void {
    this.active = this.activatedRoute.snapshot.children[0].routeConfig?.path as any;
  }

  goBack() {
    this.router.navigateByUrl('/home/checks');
  }
}

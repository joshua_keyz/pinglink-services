import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { selectNotifications } from '../../state/selector';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  notificationData$ = this.store.select(selectNotifications)
  notificationsColumns = ['Ping', 'Day', 'Time', ' ']

  ngOnInit(): void {}
  constructor(private store: Store<any>){}
}

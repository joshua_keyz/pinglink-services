import { Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-banner-card',
  templateUrl: './banner-card.component.html',
  styleUrls: ['./banner-card.component.scss']
})
export class BannerCardComponent implements OnInit{
  @Input() mainContent!: TemplateRef<any>;
  @Input() theme: 'dark' | 'light' = 'dark';

  get classes() {
    return`banner-card--${this.theme}`
  }
  ngOnInit(): void {
    
  }
}

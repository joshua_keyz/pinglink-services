import { Component, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-account-home',
  templateUrl: './account-home.component.html',
  styleUrls: ['./account-home.component.scss']
})
export class AccountHomeComponent implements DoCheck {
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}
  active: 'profile-settings' | 'subscription' = 'profile-settings';

  ngDoCheck(): void {
    this.active = this.activatedRoute.snapshot.children[0].routeConfig?.path as any;
  }

  goBack() {
    this.router.navigateByUrl('/home/checks');
  }
}

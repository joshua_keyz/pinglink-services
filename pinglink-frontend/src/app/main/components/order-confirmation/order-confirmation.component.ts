import { Component, OnInit } from "@angular/core";
import { LiqpayService } from "../../services/liqpay.service";
import { Store } from "@ngrx/store";
import { confirmPaymentRequest } from "../../state/actions";
import { selectOrderConfirmationError, selectOrderConfirmationProccess } from "../../state/selector";
import { Router } from "@angular/router";

@Component({
  selector: 'app-order-confirmation',
  styleUrls: ['./order-confirmation.component.scss'],
  templateUrl: './order-confirmation.component.html'
})
export class OrderConfirmationComponent implements OnInit {
  orderConfirmation$ = this.store.select(selectOrderConfirmationProccess);
  orderError$ = this.store.select(selectOrderConfirmationError);
  ngOnInit(): void {
    this.store.dispatch(confirmPaymentRequest())
  }
  goToChecks() {
    this.router.navigate(['/home/checks'])
  }
  constructor(private router: Router, private store: Store) {}
}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckOverviewItemComponent } from './check-overview-item.component';

describe('CheckOverviewItemComponent', () => {
  let component: CheckOverviewItemComponent;
  let fixture: ComponentFixture<CheckOverviewItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CheckOverviewItemComponent]
    });
    fixture = TestBed.createComponent(CheckOverviewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-check-overview-item',
  templateUrl: './check-overview-item.component.html',
  styleUrls: ['./check-overview-item.component.scss']
})
export class CheckOverviewItemComponent {
  @Input() value = 0;
  @Input() metric: string = '';
  @Input() theme = 'primary'
  
  get classes() {
    return `check-overview-item--${this.theme}`
  }
}

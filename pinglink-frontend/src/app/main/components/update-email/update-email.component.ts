import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Store } from "@ngrx/store";
import { setNewEmailRequest, verifyAccessRequest, verifyAccessSuccess, verifyNewEmailRequest } from "../../state/actions";
import { selectEmailChangeStep } from "../../state/selector";
import { Observable } from "rxjs";

@Component({
  selector: 'app-update-email',
  templateUrl: './update-email.component.html',
  styleUrls: ['./update-email.component.scss'],
})
export class UpdateEmailComponent {
  step: Observable<number>  = this.store.select(selectEmailChangeStep);
  verificationForm = new FormGroup({
    password: new FormControl(''),
  });
  newEmailForm = new FormGroup({
    email: new FormControl(''),
  });
  changeEmailForm = new FormGroup({
    verificationCode: new FormControl(''),
  });

  verify() {
    // this.step = 2;
    this.store.dispatch(verifyAccessRequest({password: this.verificationForm.value.password}));
  }
  verifyEmail() {
    // this.step = 3;
    this.store.dispatch(setNewEmailRequest({email: this.newEmailForm.value.email}));
  }
  changeEmail() {
    this.store.dispatch(verifyNewEmailRequest({verificationToken: this.changeEmailForm.value.verificationCode}));
  }

  constructor(private store: Store) {}
}
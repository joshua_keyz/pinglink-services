export interface WSRegistryModel {
  type: 'registry';
  payload: {
    id: string;
    email: string;
    connectionId: string;
  }
}
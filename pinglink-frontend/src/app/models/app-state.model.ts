import { AuthStateModel } from "../auth/models/auth-state.model";
import { MainStateModel } from "../main/models/main-state.model";

export interface AppStateModel {
  auth: AuthStateModel;
  main: MainStateModel;
}
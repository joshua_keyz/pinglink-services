import { Component, OnInit } from '@angular/core';
import { MenuItemModel, NotificationService } from '@ping-ui/ping-ui';
import { WSRegistryModel } from './models/ws-registry.model';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'pinglink-frontend';

  constructor(private notificationService: NotificationService) {}

  ngOnInit(): void {
    this.notificationService.sendNotification({
      variant: 'primary',
      timeout: 5000,
      content: 'Welcome',
    });
  }
}

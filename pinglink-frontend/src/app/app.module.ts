import { APP_INITIALIZER, NgModule, isDevMode } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotificationService, PingUiModule } from '@ping-ui/ping-ui';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { NotAuthGuard } from './services/not-auth-guard.service';
import { AuthGuard } from './services/auth-guard.service';
import { AuthService } from './auth/services/auth.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './services/auth-interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserService } from './main/services/user.service';
import { NotVerifiedGuard } from './services/not-verified-guard.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { OrderGuardService } from './services/order-guard.service';
import { OrderService } from './services/order.service';
import { Router } from '@angular/router';

@NgModule({
  declarations: [AppComponent, LandingPageComponent],
  imports: [
    AppRoutingModule,
    PingUiModule,
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot({}, {}),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
    EffectsModule.forRoot([]),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      registrationStrategy: 'registerWhenStable:30000',
    }),
  ],
  providers: [
    NotAuthGuard,
    AuthGuard,
    AuthService,
    NotVerifiedGuard,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    NotificationService,
    OrderGuardService,
    OrderService,
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [OrderService, Router],
      useFactory: (orderService: OrderService, router: Router) => {
        return () => {
          const orderCookie = orderService.getPaymentCookie();
          if (!orderCookie) {
            return true;
          } else {
            return router.navigate(['/home/account/order-confirmation']);
          }
        };
      },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

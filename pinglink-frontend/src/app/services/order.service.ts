import { Injectable } from "@angular/core";
import { CookieService } from "ngx-cookie-service";

@Injectable()
export class OrderService {
  setPaymentCookie(orderId: string) {
    localStorage.setItem('orderId', orderId)
  }
  removePaymentCookie() {
    localStorage.removeItem('orderId');
  }

  getPaymentCookie() {
    return localStorage.getItem('orderId');
  }

  constructor(private cookieService: CookieService) {}
}
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "../auth/services/auth.service";
import { EMPTY, Observable } from "rxjs";
import { Router } from "@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService, private router: Router) {}


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authToken = localStorage.getItem('pinglink-auth-token');

    if(!authToken && req.url.indexOf('auth') === -1) {
      alert(req.url)
      this.router.navigateByUrl('/auth')
      return EMPTY;
    }
    
    if(!authToken) {
      return next.handle(req);
    }
    const authReq = req.clone({
      setHeaders: {Authorization: `Bearer ${authToken}`}
    });

    return next.handle(authReq);
  }
}
import { Injectable } from "@angular/core";
import { Router, UrlTree } from "@angular/router";
import { AuthService } from "../auth/services/auth.service";
import { UserService } from "../main/services/user.service";
import { Observable, map } from "rxjs";

// TODO: make this check to validate the token
@Injectable()
export class NotAuthGuard {
  constructor(private router: Router, private userService: UserService) {}

  canActivate(): Observable<UrlTree> | boolean {
    const store = localStorage.getItem('pinglink-auth-token');
    if(!store) {
      return true;
    }else {
      return this.userService.getUserData().pipe(
        map(data => {
          if(!data.user) {
            localStorage.removeItem('pinglink-auth-token');
            return this.router.createUrlTree(['/'])
          }
          if(data.user?.verified) {
            return this.router.createUrlTree(['/home'])
          }
          return this.router.createUrlTree(['/auth', 'verify-email'])
        })
      )
    }
  }
}
import { Injectable } from "@angular/core";
import { Router, UrlTree } from "@angular/router";
import { AuthService } from "../auth/services/auth.service";
import { UserService } from "../main/services/user.service";
import { Observable, map } from "rxjs";

// TODO: make this check to validate the token
@Injectable()
export class NotVerifiedGuard {
  constructor(private router: Router, private userService: UserService) {}

  canActivate(): Observable<boolean|UrlTree> | UrlTree | boolean {
    if(localStorage.getItem('pinglink-auth-token')) {
      return this.userService.getUserData().pipe(
        map(data => {
          if(!data.user.verified) {
            return true
          }
          return this.router.createUrlTree(['/auth', 'signup'])
        })
      )
    }else {
      return this.router.createUrlTree(['/auth', 'signup'])
    }
  }
}
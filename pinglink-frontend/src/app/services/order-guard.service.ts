import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { OrderService } from "./order.service";
import { Observable } from "rxjs";

@Injectable()
export class OrderGuardService implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const orderCookie = this.orderService.getPaymentCookie()
    console.log(orderCookie);
    if(!orderCookie) {
      return true;
    } else {
      return this.router.createUrlTree(['/home/account/subscription']);
    }
  }
  constructor(private orderService: OrderService, private router: Router) {}
}
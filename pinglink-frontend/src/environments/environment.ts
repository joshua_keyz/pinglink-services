export const environment = {
  production: false,
  // Add Urls
  host: "http://ping.dev",
  wsHost: 'ws://ping.dev/ws/',
}
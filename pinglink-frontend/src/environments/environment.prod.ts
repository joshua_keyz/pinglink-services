export const environment = {
  production: true,
  // Add urls
  host: "https://pinglinks.joshkeys.site",
  wsHost: 'wss://pinglinks.joshkeys.site/ws/',
}
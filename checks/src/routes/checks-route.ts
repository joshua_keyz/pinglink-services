import { protect } from '@pinglink/common';
import { Router } from 'express';
import { createCheck, deleteCheck, editCheck, getChecks } from '../controllers/checks-controller';

const checksRouter = Router();

checksRouter.route('/')
  .post(protect(process.env.JWT_TOKEN!), createCheck)
  .get(protect(process.env.JWT_TOKEN!), getChecks);

checksRouter.route('/:checkId')
  .delete(protect(process.env.JWT_TOKEN!), deleteCheck)
  .put(protect(process.env.JWT_TOKEN!), editCheck)


export { checksRouter }
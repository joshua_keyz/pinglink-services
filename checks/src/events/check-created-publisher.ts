import { Publisher } from "@pinglink/common";


export class CheckCreatedPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'CHECK_CREATED'
}

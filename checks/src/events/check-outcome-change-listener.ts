import { Listener, RabbitWrapper, VerifyEmailModel } from "@pinglink/common";
import { Connection } from "amqplib";
import { Check } from "../models/checksSchema";


export class CheckChangedListener extends Listener {
  subject = 'CHECK_OUTCOME_CHANGE';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    console.log('over here', data)
    const checkId = data.checkId;
    const email = data.email;
    const state = data.state;
    
    const check = await Check.findById(checkId);
    
    await Check.updateOne({_id: checkId}, {state});
  }

  constructor(public client: Connection){
    super(client)
  }
}
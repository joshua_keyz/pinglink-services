import { Publisher } from "@pinglink/common";


export class CheckUpdatedPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'CHECK_UPDATED'
}

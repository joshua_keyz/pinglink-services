import { Publisher } from "@pinglink/common";


export class CheckDeletedPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'CHECK_DELETED'
}

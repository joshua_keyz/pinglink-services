import { Listener } from "@pinglink/common";
import { Connection } from "amqplib";
import { Check } from "../models/checksSchema";

export class UserDeletedListener extends Listener {
  subject = 'DELETE_USER_PUBLISHER';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    await Check.findByIdAndDelete(data.id);
  }

  constructor(public client: Connection){
    super(client)
  }
}
import { asyncHandler, ErrorResponse, rabbitWrapper } from "@pinglink/common";
import { Check } from "../models/checksSchema";
import { CheckCreatedPublisher } from "../events/check-created-publisher";
import { NextFunction, Request, Response } from "express";
import { CheckDeletedPublisher } from "../events/check-deleted-publisher";
import { CheckUpdatedPublisher } from "../events/check-updated-publisher";

export const createCheck = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
  const { protocol, url, method, successCodes, timeoutSeconds } = req.body;
  const {id: userId, email} = (req as any).tokenData;
  const userChecks = await Check.find({userId});

  if(userChecks.length >= 5) {
    return next(new ErrorResponse("Maximum checks reached. Upgrade from fremium to create more", 400));
  }

  const check = new Check({
    protocol: protocol.toLowerCase(), url, method: method.toLowerCase(), successCodes, timeoutSeconds, userId
  }) as any;

  await check.save();

  const checkCopy = {...check._doc, email}
  await new CheckCreatedPublisher(rabbitWrapper.client).publish(JSON.stringify(checkCopy));
  res.json({
    success: true,
    check: check._doc,
  })
});
export const deleteCheck = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
  const { checkId } = req.params;
  const {id: userId, email} = (req as any).tokenData;
  const check = await Check.findOneAndDelete({userId, _id: checkId});
  await new CheckDeletedPublisher(rabbitWrapper.client).publish(JSON.stringify(check));
  res.json({
    success: true,
  })
});

export const getChecks = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id: userId } = (req as any).tokenData;
  const checks = await Check.find({userId});

  res.json({
    success: true,
    length: checks.length,
    checks,
  })
});

export const editCheck = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const { id: userId } = (req as any).tokenData;
  const { checkId } = req.params;
  const updateData: any = {};
  if(req.body.url) {
    updateData.url = req.body.url;
  }
  if(req.body.protocol) {
    updateData.protocol = req.body.protocol.toLowerCase();
  }
  if(req.body.method) {
    updateData.method = req.body.method.toLowerCase();
  }
  if(req.body.successCodes) {
    updateData.successCodes = [req.body.successCodes]
  }
  if(req.body.timeoutSeconds) {
    updateData.timeoutSeconds = req.body.timeoutSeconds;
  }
  const check = await Check.findOneAndUpdate({_id: checkId, userId}, updateData, {
    new: true,
    runValidators: true,
  })

  if(!check) {
    return res.status(400).json({
      success: false,
    })
  }
  await new CheckUpdatedPublisher(rabbitWrapper.client).publish(JSON.stringify(check));
  res.json({
    success: true,
    check,
  })
});
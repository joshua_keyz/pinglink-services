import { Schema, model } from "mongoose";

const checkSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  protocol: {
    type: String,
    required: true,
    enum: ['http', 'https']
  },
  successCodes: {
    type: [String],
    required: true,
  },
  url: {
    type: String,
    required: true,
  },
  method: {
    type: String,
    required: true,
    enum: ['post', 'get', 'put', 'delete']
  },
  state: {
    type: String,
    enum: ['up', 'down'],
    default: 'up'
  },
  timeoutSeconds: {
    type: Number,
    required: true,
    min: 1,
    max: 5,
  }
});

const Check = model('check', checkSchema);

export { Check }
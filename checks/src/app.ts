import express from 'express';
import { checksRouter } from './routes/checks-route';

const app = express();

app.use(express.json());

app.use('/api/checks', checksRouter)

export { app };
import {
  connectDb,
  connectRabbit,
  errorHandler,
  rabbitWrapper,
} from '@pinglink/common';
import { app } from './app';
import { CheckChangedListener } from './events/check-outcome-change-listener';
import { UserDeletedListener } from './events/user-deleted-listener';

connectDb(process.env.MONGODB_URI!, 'pinglink-checks', 'Checks Service');
connectRabbit(rabbitWrapper, 'amqp://rabbitmq-srv', () => {
  new CheckChangedListener(rabbitWrapper.client).listen();
  new UserDeletedListener(rabbitWrapper.client).listen();
});


app.use(errorHandler);

app.listen(3000, () => {
  console.log('Listening on port 3000!');
});

import { asyncHandler } from "@pinglink/common";
import { NextFunction, Request, Response } from "express";

export const fetchNotifications = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
  res.send({})
})
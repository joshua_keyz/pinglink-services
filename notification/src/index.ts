import { connectDb, connectRabbit, rabbitWrapper } from '@pinglink/common';
import express from 'express';
import { CheckChangedListener } from './events/check-outcome-changed-listener';
import { FetchNotificationsListener } from './events/fetch-notifications-listener';
import { UserDeletedListener } from './events/user-deleted-listener';


const app = express();
connectDb(
  process.env.MONGODB_URI!,
  'pinglink-notification',
  'Notification Service'
);
connectRabbit(rabbitWrapper, 'amqp://rabbitmq-srv', () => {
  new CheckChangedListener(rabbitWrapper.client).listen();
  new FetchNotificationsListener(rabbitWrapper.client).listen();
  new UserDeletedListener(rabbitWrapper.client).listen();
});
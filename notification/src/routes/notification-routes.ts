import { Router } from 'express';
import { fetchNotifications } from '../controllers/notification.controller';
import { protect } from '@pinglink/common';

const checksRouter = Router();

checksRouter.route('/').get(protect(process.env.JWT_TOKEN!), fetchNotifications);

export { checksRouter }
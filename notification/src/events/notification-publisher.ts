import { Publisher } from "@pinglink/common";


export class NotificationPublisher extends Publisher {
  exchange = 'pinglink';
  
  subject = 'NOTIFICATION';
}

import { Listener, RabbitWrapper, VerifyEmailModel, rabbitWrapper } from "@pinglink/common";
import { Connection } from "amqplib";
import { Notification } from "../models/notification-schema";
import { NotificationPublisher } from "./notification-publisher";



export class CheckChangedListener extends Listener {
  subject = 'CHECK_OUTCOME_CHANGE';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    console.log('checked was changed at notification service')
    const checkId = data.checkId;
    const checkState = data.state;
    const email = data.email;
    const checkUrl = data.checkUrl;
    const userId = data.userId;


    const notification = new Notification({
      userId,
      checkId,
      checkState,
      checkUrl
    })

    const notificationRes = await notification.save();
    await new NotificationPublisher(rabbitWrapper.client).publish(JSON.stringify({
      ...(notificationRes as any)._doc,
      email,
    }));
  }

  constructor(public client: Connection) {
    super(client)
  }
}
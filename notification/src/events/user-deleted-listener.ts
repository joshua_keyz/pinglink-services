import { Listener } from "@pinglink/common";
import { Connection } from "amqplib";
import { Notification } from "../models/notification-schema";

export class UserDeletedListener extends Listener {
  subject = 'DELETE_USER_PUBLISHER';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    await Notification.deleteMany({userId: data.id});
  }

  constructor(public client: Connection){
    super(client)
  }
}
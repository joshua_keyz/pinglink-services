import { Publisher } from "@pinglink/common";


export class NotificationResultPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'NOTIFICATION_RESULT';
}

import { Listener, rabbitWrapper } from "@pinglink/common";
import { Connection } from "amqplib";
import { Notification } from "../models/notification-schema";
import { NotificationResultPublisher } from "./notification-result-publisher";


export class FetchNotificationsListener extends Listener {
  subject = 'FETCH_NOTIFICATION';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    const {
      userId,
      email,
      connectionId,
    } = data;
    const notifications = await Notification.find({userId});
    await new NotificationResultPublisher(rabbitWrapper.client).publish(
      JSON.stringify({email, notifications, connectionId})
    );
    
  }

  constructor(public client: Connection){
    super(client)
  }
}
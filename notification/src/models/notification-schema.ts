import { Schema, model } from "mongoose";

const notificationSchema = new Schema({
  checkId: {
    type: String
  },
  checkUrl: {
    type: String,
    // required: true,
  },
  userId: {
    type: String,
  },
  opened: {
    type: Boolean,
    default: false,
  },
  notificationTime: {
    type: Date,
    default: Date.now,
  },
  checkState: {
    type: String,
    enum: ['up', 'down'],
    required: true,
  },
});

export const Notification = model('pinglink-notification', notificationSchema);
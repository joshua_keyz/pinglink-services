import express from 'express';

const app = express();
app.listen(3000, () => {
  console.log('Notification service listening at port 3000');
});

export { app }

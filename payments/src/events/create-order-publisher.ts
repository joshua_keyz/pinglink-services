import { Publisher } from "@pinglink/common";


export class CreateOrderPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'CREATE_ORDER_PUBLISHER'
}

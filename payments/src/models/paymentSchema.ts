import mongoose, { Schema, model } from "mongoose";

const paymentSchema = new mongoose.Schema({
    paymentStatus: {
        type: String,
        required: true,
    },
    userId: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    orderId: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    amount: {
        type: String,
        required: true,
    },
    subscription: {
        type: String,
        default: 'freemium',
        enum: ['freemium', 'professional', 'enterprise']
    },
    createAt: {
        type: Date,
        default: Date.now
    }
});

export const Payment = model('payment', paymentSchema);
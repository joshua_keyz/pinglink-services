import { Router } from "express";
import { fulfillPayment, getPaymentSignature } from "../controllers/controllers";
import { protect } from "@pinglink/common";

const router = Router();

router.route('/payment-signature').get(getPaymentSignature);
router.route('/fulfil-payment').post(protect(process.env.JWT_TOKEN!), fulfillPayment);

export { router };
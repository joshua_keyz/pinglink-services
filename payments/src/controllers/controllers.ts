import { TokenModel, asyncHandler, rabbitWrapper } from "@pinglink/common";
import { NextFunction, Request, Response } from "express";
import { createHash } from "crypto";
import Liqpay from 'liqpayjs-sdk';
import { CreateOrderPublisher } from "../events/create-order-publisher";
import { Payment } from "../models/paymentSchema";

export const getPaymentSignature = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { order_id, amount, description, info, result_url } = req.query;

    const json_string = {
      public_key: process.env.LIQPAY_PUBLIC_KEY!,
      action: "pay",
      amount,
      currency: "USD",
      description,
      order_id,
      version: "3",
      info,
      result_url,
    };

    const buff = Buffer.from(JSON.stringify(json_string));
    const data = buff.toString("base64");
    const privateKey = process.env.LIQPAY_PRIVATE_KEY!;

    const sign_string = privateKey.trim() + data + privateKey.trim();
    const sha = createHash("sha1");
    sha.update(sign_string);
    const signature = sha.digest("base64");
    console.log(signature);
    res.json({ signature, data, sign_string });
  }
);

export const fulfillPayment = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
  const { order_id } = req.body;
  const tokenData: TokenModel = (req as any).tokenData;

  const liqPay = new Liqpay(
    process.env.LIQPAY_PUBLIC_KEY!,
    process.env.LIQPAY_PRIVATE_KEY!
  );

  liqPay.api('request', {
    'action': 'status',
    'version': '3',
    order_id
  }, async function (json: any) {
    const paymentStatus = json.status;
    const orderId = json.order_id;
    const description = json.description;
    const userId = tokenData.id;
    const amount = json.amount;
    let subscription;

    console.log(order_id)

    if(amount == '0') {
      subscription = 'freemium';
    } else if(amount == '10') {
      subscription = 'professional'
    } else {
      subscription = 'enterprise';
    }

    if(paymentStatus === 'success') {
      console.log('Payment was successful')
      await new CreateOrderPublisher(rabbitWrapper.client).publish(
        JSON.stringify({
          paymentStatus,
          orderId,
          description,
          amount,
          userId,
          subscription,
        })
      );
      const existingUserPaymentProfile = await Payment.findOne({
        userId
      });
      if(!existingUserPaymentProfile) {
        const payment = new Payment({
          paymentStatus,
          orderId,
          description,
          amount,
          userId,
          subscription,
        });
        await payment.save();
      } else {
        existingUserPaymentProfile.orderId = orderId;
        existingUserPaymentProfile.description = description;
        existingUserPaymentProfile.amount = amount;
        existingUserPaymentProfile.subscription = subscription;
      }
      res.status(200).json({
        success: true,
        subscription
      });
    } else {
      console.log(json);
      res.status(400).json({
        success: false,
        error: json.err_code
      })
    }
  });
})
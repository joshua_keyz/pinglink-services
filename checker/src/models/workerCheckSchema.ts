import { Schema, model } from "mongoose";

const checkerSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  email: {
    type: String,
    required: true,
    match: [/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, 'Please provide a valid email address'],
  },
  protocol: {
    type: String,
    required: true,
    enum: ['http', 'https']
  },
  url: {
    type: String,
    required: true,
  },
  method: {
    type: String,
    required: true,
    enum: ['post', 'get', 'put', 'delete'],
  },
  successCodes: {
    type: [String],
    required: true,
  },
  timeoutSeconds: {
    type: Number,
    required: true,
    min: 1,
    max: 5,
  },
  state: {
    type: String,
    enum: ['up', 'down'],
    default: 'up'
  },
  lastChecked: {
    type: Date,
  }
});

export const WorkerCheck = model('worker-check', checkerSchema)
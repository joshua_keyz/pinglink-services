import { WorkerCheck } from './models/workerCheckSchema';
import { parse } from 'url';
import http from 'http';
import https from 'https';
import { CheckOutcomeChangePublisher } from './events/CheckOutcomeChangePublisher';
import { rabbitWrapper } from '@pinglink/common';
import { SendPushNotificationPublisher } from './events/PushNotificationPublisher';
import { PushNotificationPayloadModel } from './models/push-notification-payload.interface';


export class Worker {
  loop() {
    setInterval(() => {
      this.gatherAllChecks();
    }, 1000 * 60)
  }

  async gatherAllChecks() {
    // Get all the checks that exists in the system
    const checks = await WorkerCheck.find({});

    if(checks.length > 0) {
      checks.forEach(check => {
        this.performCheck(check)
      })
    }
  }

  async performCheck(check: any) {
    // Prepare the initial check outcome
    const checkOutcome = {
      error: null,
      responseCode: 0,
    }

    // Mark that the outcome has not been sent yet
    let outcomeSent = false;

    // Parse the hostname and the path out of the original check data
    let parsedUrl = parse(check.protocol + '://' + check.url, true);
    const hostname = parsedUrl.hostname;
    const path = parsedUrl.path;

    // Construct the request
    let requestDetails = {
      protocol: check.protocol+':',
      hostname,
      method: check.method.toUpperCase(),
      path,
      timeout: check.timeoutSeconds * 1000
    };

    // Instantiate the request object (using either the http or https module)
    let _moduleToUse = check.protocol === 'http' ? http : https;

    let req = _moduleToUse.request(requestDetails, res => {
      let status = res.statusCode as number;

      // Update the checkoutcome and pass the data along
      checkOutcome.responseCode = status;
      if(!outcomeSent) {
        this.processCheckOutcome(check, checkOutcome);
        outcomeSent = true;
      }
    });

    req.on('error', (err:any ) => {
      // Update the checkOutcome and pass the data along
      checkOutcome.error = {'error': true, value: err} as any;
      if(!outcomeSent) {
        this.processCheckOutcome(check, checkOutcome);
        outcomeSent = true;
      }
    });
  
    // Bind to the timeout event
    req.on('timeout', () => {
      // Update the checkOutcome and pass the data along
      checkOutcome.error = {'error': true, value: 'timeout'} as any;
      if(!outcomeSent) {
        this.processCheckOutcome(check, checkOutcome);
        outcomeSent = true;
      }
    });

    req.end();
  }

  async processCheckOutcome(originalCheckData: any, checkOutcome: any) {
    console.log(checkOutcome)
    // Decide if the check is considered up or down
    const state = !checkOutcome.error &&
      checkOutcome.responseCode &&
      originalCheckData.successCodes.indexOf(checkOutcome.responseCode) > -1
        ? 'up'
        : 'down';
    
    const alertWarranted = // originalCheckData.lastChecked && 
    originalCheckData.state !== state
      ? true
      : false;
  


    // It's questionable if this should be called here.
    await WorkerCheck.updateOne({_id: originalCheckData._id}, {state, lastChecked: Date.now()});

    if(alertWarranted) {
      const newCheckData = {...originalCheckData._doc};
      newCheckData.state = state;
      newCheckData.lastChecked = Date.now();
      console.log('Check data was changed')
      this.alertUserToStatusChange(newCheckData);
    } else {
      console.log('Check outcome has not changed, no alert needed');
    }
  }
  alertUserToStatusChange(newCheckData: any) {
    const msgBody =
    "Your check for " +
    newCheckData.method.toUpperCase() +
    " " +
    newCheckData.protocol +
    "://" +
    newCheckData.url +
    ' is currently ' + newCheckData.state.toUpperCase();

    // Send this message to the messenger service and update check data
    new CheckOutcomeChangePublisher(rabbitWrapper.client).publish(
      JSON.stringify({
        msgBody,
        email: newCheckData.email,
        checkId: newCheckData._id,
        checkUrl: newCheckData.url,
        state: newCheckData.state,
        userId: newCheckData.userId
      })
    );
    const notificationPayload: PushNotificationPayloadModel = {
      notification: {
        title: 'PingLink',
        body: 'A check status was changed',
        icon: 'https://pinglink-spaces.fra1.digitaloceanspaces.com/pinglink-assets/icon-logo.svg',
        vibrate: [100, 50, 100],
        data: {
          dateOfArrival: Date.now(),
          primaryKey: 1
        },
        click_action: "https://pinglinks.keyssoft.site",
        actions: [{
          action: 'explore',
          title: 'Go to the site'
        }]
      }
    }
    new SendPushNotificationPublisher(rabbitWrapper.client).publish(
      JSON.stringify({notificationPayload, userId: newCheckData.userId})
    );
  }
}
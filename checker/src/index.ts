import { connectDb, connectRabbit, rabbitWrapper } from '@pinglink/common';
import { Worker } from './app';
import { CheckCreatedListener } from './events/CheckCreatedListener';
import { CheckDeletedListener } from './events/CheckDeletedListener';
import { CheckUpdatedListener } from './events/CheckUpdatedListener';
import { UserDeletedListener } from './events/user-deleted-listener';

connectDb(process.env.MONGODB_URI!, 'pinglink-worker-checks', 'Checks Service');

connectRabbit(rabbitWrapper, 'amqp://rabbitmq-srv', () => {
  const worker = new Worker();
  new CheckUpdatedListener(rabbitWrapper.client).listen();
  new CheckCreatedListener(rabbitWrapper.client).listen();
  new CheckDeletedListener(rabbitWrapper.client).listen();
  new UserDeletedListener(rabbitWrapper.client).listen();
  worker.gatherAllChecks();
  worker.loop();
});

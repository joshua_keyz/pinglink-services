import { Publisher } from "@pinglink/common";


export class CheckOutcomeChangePublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'CHECK_OUTCOME_CHANGE'
}

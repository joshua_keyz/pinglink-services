import { Listener } from "@pinglink/common";
import { WorkerCheck } from "../models/workerCheckSchema";
import { Connection } from "amqplib";

export class UserDeletedListener extends Listener {
  subject = 'DELETE_USER_PUBLISHER';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    await WorkerCheck.findByIdAndDelete(data.id);
  }

  constructor(public client: Connection){
    super(client)
  }
}
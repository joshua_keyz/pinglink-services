import { Publisher } from "@pinglink/common";


export class SendPushNotificationPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'SEND_PUSH_NOTIFICATION'
}

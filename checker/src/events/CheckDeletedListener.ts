import { Listener, RabbitWrapper, VerifyEmailModel } from "@pinglink/common";
import { Connection } from "amqplib";
import { WorkerCheck } from "../models/workerCheckSchema";


export class CheckDeletedListener extends Listener {
  subject = 'CHECK_DELETED';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    await WorkerCheck.findByIdAndDelete(data._id);
  }

  constructor(public client: Connection){
    super(client)
  }
}
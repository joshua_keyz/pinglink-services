import { Listener, RabbitWrapper, VerifyEmailModel } from "@pinglink/common";
import { Connection } from "amqplib";
import { WorkerCheck } from "../models/workerCheckSchema";


export class CheckCreatedListener extends Listener {
  subject = 'CHECK_CREATED';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    const workerCheck = new WorkerCheck({...data});
    await workerCheck.save();
  }

  constructor(public client: Connection){
    super(client)
  }
}
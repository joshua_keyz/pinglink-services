import { Listener, RabbitWrapper, VerifyEmailModel } from "@pinglink/common";
import { Connection } from "amqplib";
import { WorkerCheck } from "../models/workerCheckSchema";


export class CheckUpdatedListener extends Listener {
  subject = 'CHECK_UPDATED';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    await WorkerCheck.findByIdAndUpdate(data._id, {
      protocol: data.protocol.toLowerCase(),
      url: data.url, 
      method: data.method.toLowerCase(),
      successCodes: data.successCodes,
      timeoutSeconds: data.timeoutSeconds
    });
  }

  constructor(public client: Connection){
    super(client)
  }
}
import { Listener } from "@pinglink/common";
import { Connection } from "amqplib";


export class NotificationResultListener extends Listener {
  subject = 'NOTIFICATION_RESULT';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    console.log(this.connections.length);
    const {
      notifications,
      email,
      connectionId,
    } = data;

    const userIdx = this.connections.findIndex(conn => 
      conn.customData.connectionId === connectionId
    );
    if(userIdx > -1) {
      this.connections[userIdx].send(
        JSON.stringify({
          type: "notification",
          payload: notifications,
        })
      );
    }
  }

  constructor(public client: Connection, private connections: any[]){
    super(client)
  }
}
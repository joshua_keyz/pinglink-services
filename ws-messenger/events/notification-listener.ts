import { Listener } from "@pinglink/common";
import { Connection } from "amqplib";


export class NotificationListener extends Listener {
  subject = 'NOTIFICATION';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    console.log('Hello World')
    const {
      userId,
      checkId,
      checkState,
      checkUrl,
      _id,
      notificationTime,
      email,
    } = data;

    const connections = this.connections.filter(conn => 
      conn.customData.email === email
    );
    console.log(connections.length);
    if(connections.length > 0) {
      this.connections.forEach(connection => {
        connection.send(
          JSON.stringify({
            type: "new-notification",
            payload: {
              userId,
              checkId,
              checkState,
              checkUrl,
              _id,
              notificationTime,
            },
          })
        )
      });
    }
  }

  constructor(public client: Connection, private connections: any[]){
    super(client)
  }
}
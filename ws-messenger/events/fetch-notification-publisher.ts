import { Listener, Publisher } from "@pinglink/common";
import { Connection } from "amqplib";


export class FetchNotificationPublisher extends  Publisher{
  subject = 'FETCH_NOTIFICATION';
  exchange = 'pinglink'
}
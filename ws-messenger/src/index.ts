import { connectRabbit, rabbitWrapper } from '@pinglink/common';
import { createServer } from 'http';
import { Server } from 'ws';
import express from 'express';
import { WSMessageModel } from './models/ws-message.model';
import { NotificationListener } from '../events/notification-listener';
import { FetchNotificationPublisher } from '../events/fetch-notification-publisher';
import { NotificationResultListener } from '../events/notification-result.listener';

const connections: any[] = [];
const app = express();
const httpServer = createServer(app)
connectRabbit(rabbitWrapper, 'amqp://rabbitmq-srv', () => {
  new NotificationListener(rabbitWrapper.client, connections).listen();
  new NotificationResultListener(rabbitWrapper.client, connections).listen();
  initWS();
});
function initWS() {
  const websocket = new Server({ server: httpServer });
  websocket.on('open', () => {});
  websocket.on('connection', (socket, req) => {
    socket.on('close', () => {
      const connectionToRemove = connections.findIndex((conn) => {
        return conn.id === (socket as any).id;
      });
      connections.splice(connectionToRemove, 1);
    });

    socket.on('message', async (data: any) => {
      const parsedData: WSMessageModel = JSON.parse(JSON.parse(data));
      switch (parsedData.type) {
        case 'registry': {
          (socket as any).customData = {
            email: parsedData.payload.email,
            id: parsedData.payload.id,
            connectionId: parsedData.payload.connectionId,
          };

          connections.push(socket);
          await new FetchNotificationPublisher(rabbitWrapper.client).publish(JSON.stringify({
            userId: parsedData.payload.id,
            email: parsedData.payload.email,
            connectionId: parsedData.payload.connectionId,
          }));
          break;
        }

        default: {
          console.log(parsedData.type);
        }
      }
    });
  });

  httpServer.listen(3000, () => {
    console.log('WebSocket HttpServer running on port 3000!!!');
  });
}

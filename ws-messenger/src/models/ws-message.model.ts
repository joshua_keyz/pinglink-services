export interface WSMessageModel {
  type: 'registry' | 'fetch-notifications';
  payload: any;
}

export interface RegistryModel extends WSMessageModel {
  type: 'registry';
  payload: {
    id: string;
    email: string;
  }
}
import { connectRabbit, rabbitWrapper } from '@pinglink/common';
import { run } from './utils/email-verification-template';
import { sendEmailByTemplate } from './utils/send-email-template';
import { VerifyEmailListener } from './events/verify-email-listener';
import { forgotPasswordTemplate } from './utils/forgot-password-template';
import { verifyEmailTemplate } from './utils/verify-email-template';
import { ResetPasswordEmailListener } from './events/reset-password-email-listener';
import { checkDownEmailTemplate } from './utils/check-down-email-template';
import { checkUpEmailTemplate } from './utils/check-up-email-template';
import { CheckChangedListener } from './events/check-outcome-change-listener';


connectRabbit(rabbitWrapper, 'amqp://rabbitmq-srv', () => {
  run(
    'ForgotPasswordTemplate',
    
    forgotPasswordTemplate,
    'Pinglink - Reset Password'
  );
  run(
    'EmailVerificationTemplate',
    verifyEmailTemplate,
    'Pinglink - Email Verification'
  );
  run(
    'CheckDownEmailTemplate',
    checkDownEmailTemplate,
    'Pinglink - Check is down'
  );
  run('CheckUpEmailTemplate', checkUpEmailTemplate, 'Pinglink - Check is up');

  new VerifyEmailListener(rabbitWrapper.client).listen();
  new ResetPasswordEmailListener(rabbitWrapper.client).listen();
  new CheckChangedListener(rabbitWrapper.client).listen();
});

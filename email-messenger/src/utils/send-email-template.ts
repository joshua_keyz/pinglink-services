import {SESClient, SESClientConfig, SendTemplatedEmailCommand} from '@aws-sdk/client-ses';

const SES_CONFIG: SESClientConfig = {
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID!,
    secretAccessKey: process.env.AWS_ACCESS_KEY_SECRET!
  },
  region: process.env.AWS_SES_REGION
};

const sesClient = new SESClient(SES_CONFIG);

export const sendEmailByTemplate = async(templateName: string, recipientEmail: string, TemplateData: string) => {
  const sendTemplatedEmailCommand = new SendTemplatedEmailCommand({
    Source: "avwerosuoghene.oguma1@gmail.com",
    Destination: {
      ToAddresses: [
        recipientEmail
      ],
    },
    Template: templateName,
    TemplateData,
  })

  try {
    const res = await sesClient.send(sendTemplatedEmailCommand);
    console.log('Email with SES template has been sent successfully!', res);
  } catch (error) {
    console.error(error);
  }
};

// sendEmail('SES-Template', 'joshua.oguma@outlook.com')
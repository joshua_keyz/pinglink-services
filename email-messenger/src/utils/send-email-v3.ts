import {SESClient, SESClientConfig, SendEmailCommand} from '@aws-sdk/client-ses';

const SES_CONFIG: SESClientConfig = {
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID!,
    secretAccessKey: process.env.AWS_ACCESS_KEY_SECRET!
  },
  region: process.env.AWS_SES_REGION
};

const sesClient = new SESClient(SES_CONFIG);

export const sendEmail = async (recipientEmail: string, subject: string, body: string) => {
  let params = {
    Source: "avwerosuoghene.oguma1@gmail.com",
    Destination: {
      ToAddresses: [
        recipientEmail
      ],
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: body
        },
        Text: {
          Charset: 'UTF-8',
          Data: 'This is the body of my email'
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: subject
      }
    }
  };

  try {
    const sendEmailCommand = new SendEmailCommand(params);
    const res = await sesClient.send(sendEmailCommand);
    // const res = await AWS_SES.sendEmail(params).promise();
    console.log('Email has been sent!', res)
  } catch (error) {
    console.log(error);
  }
}
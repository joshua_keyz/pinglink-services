import { SES } from 'aws-sdk';
const SES_CONFIG = {
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_ACCESS_KEY_SECRET,
  region: process.env.AWS_SES_REGION,
};
const AWS_SES = new SES(SES_CONFIG);
export const sendEmail = async (recipientEmail: string, name: string) => {
  let params = {
    Source: 'avwerosuoghene.oguma1@gmail.com',


    Destination: {
      ToAddresses: [recipientEmail],
    },
    Message: {
      Body: {
        Html: {
          Charset: 'UTF-8',
          Data: '<h1>This is the body of my email!</h1>',
        },
        Text: {
          Charset: 'UTF-8',
          Data: 'This is the body of my email',
        },
      },
      Subject: {
        Charset: 'UTF-8',
        Data: `Hello, ${name}`,
      },
    },
  };

  try {
    const res = await AWS_SES.sendEmail(params).promise();
    console.log('Email has been sent!', res);
  } catch (error) {
    console.log(error);
  }
};

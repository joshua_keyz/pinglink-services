import { SESClient, CreateTemplateCommand, SESClientConfig, GetTemplateCommand, TemplateDoesNotExistException, DeleteTemplateCommand } from '@aws-sdk/client-ses';

const SES_CONFIG: SESClientConfig = {
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID!,
    secretAccessKey: process.env.AWS_ACCESS_KEY_SECRET!
  },
  region: process.env.AWS_SES_REGION
};

const sesClient = new SESClient(SES_CONFIG);

export const run = async (template_name: string, template_content: string, templateSubject: string) => {
  const createTemplateCommand = new CreateTemplateCommand({
    Template: {
      TemplateName: template_name,
      HtmlPart: template_content,
      SubjectPart: templateSubject,
    }
  });

  try {
    const command = new GetTemplateCommand({
      TemplateName: template_name
    });
    
    const response = await sesClient.send(command);
  } catch (error) {
    if(error instanceof TemplateDoesNotExistException) {
      try {
        const res = await sesClient.send(createTemplateCommand);
        console.log('SES template has been created', res)
      } catch (error) {
        console.error('Failed to create template', error)
      }
    }
  }  
}
import { Listener, RabbitWrapper, VerifyEmailModel } from "@pinglink/common";
import { sendEmail } from "../utils/send-email-v3";
import { Connection } from "amqplib";
import { sendEmailByTemplate } from "../utils/send-email-template";


export class VerifyEmailListener extends Listener {
  subject = 'VERIFY_EMAIL_REQUEST';
  exchangeName = 'pinglink'

  onMessage = async (data: VerifyEmailModel) => {
    console.log('Got an event and currently sending email')
    const { email, verificationCode } = data;
    await sendEmailByTemplate('EmailVerificationTemplate', email, JSON.stringify({verificationCode}));
  }

  constructor(public client: Connection){
    super(client)
  }
}
import { Listener, } from "@pinglink/common";
import { Connection } from "amqplib";
import { sendEmailByTemplate } from "../utils/send-email-template";


export class ResetPasswordEmailListener extends Listener {
  subject = 'RESET_PASSWORD_EMAIL_REQUEST';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    const { email, resetPasswordCode } = data;
    await sendEmailByTemplate('ForgotPasswordTemplate', email, JSON.stringify({resetPasswordCode}));
  }

  constructor(public client: Connection){
    super(client)
  }
}
import { Listener } from "@pinglink/common";
import { Connection } from "amqplib";
import { sendEmailByTemplate } from "../utils/send-email-template";


export class CheckChangedListener extends Listener {
  subject = 'CHECK_OUTCOME_CHANGE';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    const { state, email, msgBody } = data;
    const template = state.toLowerCase() === 'up' ? 'CheckUpEmailTemplate' : 'CheckDownEmailTemplate'
    await sendEmailByTemplate(template, email, JSON.stringify({alertMsg: msgBody}));
  }

  constructor(public client: Connection){
    super(client)
  }
}
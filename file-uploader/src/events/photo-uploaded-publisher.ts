import { Publisher } from "@pinglink/common";


export class PhotoUploadedPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'PHOTO_UPLOADED_PUBLISHER'
}

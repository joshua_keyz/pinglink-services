import express, { Request } from 'express';

import { protect } from '@pinglink/common';
import { uploadPhoto } from '../controllers/file-uploader-controller';

const fileUploaderRoute = express.Router();

fileUploaderRoute.route('/upload-photo').post(protect(process.env.JWT_TOKEN!), uploadPhoto);

export { fileUploaderRoute };
import express from 'express';
import { fileUploaderRoute } from './routes/file-uploader-route';

const app = express();
app.use(express.json());


app.use('/api/file-uploader', fileUploaderRoute);

export { app };

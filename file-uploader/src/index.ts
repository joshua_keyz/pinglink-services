import {
  connectRabbit,
  errorHandler,
  rabbitWrapper,
  RabbitWrapper,
} from '@pinglink/common';
import { app } from './app';

connectRabbit(rabbitWrapper, 'amqp://rabbitmq-srv', () => {});
app.use(errorHandler);

app.listen(3000, () => {
  console.log('Listening on port 3000!!!!');
});

import { asyncHandler, rabbitWrapper } from "@pinglink/common";
import { NextFunction, Request, Response } from "express";
import { PutObjectCommand, S3Client } from '@aws-sdk/client-s3';
import formidable from 'formidable';
import { readFileSync } from 'fs';
import { PhotoUploadedPublisher } from "../events/photo-uploaded-publisher";
import { v4 as uuidv4 } from 'uuid';

export const uploadPhoto = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
  const tokenData = (req as any).tokenData;
  const s3Client = new S3Client({
    forcePathStyle: true,
    region: 'fra1',
    endpoint: 'https://fra1.digitaloceanspaces.com',
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY!,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY!
    }
  });

  const form = formidable({});
  form.parse(req, async (err, fields, files) => {
    if (err) {
      next(err);
      return;
    };
    console.log(tokenData);
    const file = readFileSync(files.fileName[0].filepath);
    const fileName = uuidv4();
    const params = {
      Bucket: "pinglink-spaces",
      Key: `pinglink-assets/${fileName}.png`,//pinglink-spaces
      Body: file,
      ACL: "public-read",
    };
    const uploadObject = async () => {
      try {
        console.log('Hello World')
        const data = await s3Client.send(new PutObjectCommand(params as any));

        const url = `https://${params.Bucket}.fra1.digitaloceanspaces.com/${params.Key}`;

        await new PhotoUploadedPublisher(rabbitWrapper.client).publish(JSON.stringify({url, id: tokenData.id}));
        console.log(
          "Successfully uploaded object: " + params.Bucket + "/" + params.Key
        );
        return url;
      } catch (err) {
        console.log("Error", err);
      }
    };

    const url = await uploadObject();
    res.status(200).json({success: true, url});
  });

  
  
  // res.send('Hello')
});


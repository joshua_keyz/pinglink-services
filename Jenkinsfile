pipeline {
    agent any
    environment {
        GIT_AUTHOR_NAME = 'Joshua Avwerosuoghene Oguma'
        GIT_AUTHOR_EMAIL = 'joshua.oguma@outlook.com'
        NPM_TOKEN = credentials('NPM_TOKEN')
        GL_TOKEN = credentials('GL_TOKEN')
        DO_TOKEN = credentials('DOCTL_ACCESS_TOKEN')
    }

    options {
        gitLabConnection('pingui')
    }
    
    stages {
        stage('Infra') {
            when { changeset "infra/**"}
            stages {
                stage('Deploy Infra') {
                    steps {
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl apply -f infra/k8s-prod --context do-tor1-pinglink-services && kubectl apply -f infra/k8s --context do-tor1-pinglink-services'
                        }
                    }
                }
            }
        }
        stage('Auth') {
            when { changeset "auth/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd auth && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd auth && docker build -t keysoutsourcedocker/pinglink-auth .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-auth'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment auth-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }
        stage('Notification') {
            when { changeset "notification/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd notification && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd notification && docker build -t keysoutsourcedocker/pinglink-notification .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-notification'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment notification-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }
        stage('Push-Notification') {
            when { changeset "push-notification/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd push-notification && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd push-notification && docker build -t keysoutsourcedocker/pinglink-push-notification .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-push-notification'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment push-notification-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }
        stage('Checks') {
            when { changeset "checks/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd checks && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd checks && docker build -t keysoutsourcedocker/pinglink-checks .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-checks'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment checks-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }
        stage('Payments') {
            when { changeset "payments/**" }
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd payments && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd payments && docker build -t keysoutsourcedocker/pinglink-payments .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-payments'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment payments-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }
        stage('Email Messenger') {
            when { changeset "email-messenger/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd email-messenger && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd email-messenger && docker build -t keysoutsourcedocker/pinglink-email-messenger .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-email-messenger'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment email-messenger-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }
        stage('File Uploader') {
            when { changeset "file-uploader/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd file-uploader && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd file-uploader && docker build -t keysoutsourcedocker/pinglink-file-uploader .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-file-uploader'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment file-uploader-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }

        stage('Worker') {
            when { changeset "checker/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd checker && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd checker && docker build -t keysoutsourcedocker/pinglink-worker .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-worker'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment worker-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }
        stage('WS-Messenger') {
            when { changeset "ws-messenger/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd ws-messenger && npm install'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd ws-messenger && docker build -t keysoutsourcedocker/pinglink-ws-messenger .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-ws-messenger'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment ws-messenger-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
            }
        }

        stage('Frontend') {
            when { changeset "pinglink-frontend/**"}
            stages {
                stage('Install Deps') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'cd pinglink-frontend && npm install --force'
                        }
                        script {
                            updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                        }
                    }
                }
                stage('Build') {
                    steps {
                        sh 'cd pinglink-frontend && docker build -t keysoutsourcedocker/pinglink-frontend .'
                        withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDS', passwordVariable: 'DOCKER_PASSWORD', usernameVariable: 'DOCKER_USERNAME')]) {
                            sh 'docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}'
                            sh 'docker push keysoutsourcedocker/pinglink-frontend'
                            sh 'doctl auth init --access-token ${DO_TOKEN}'
                            sh 'doctl kubernetes cluster kubeconfig save pinglink-services'
                            sh 'kubectl rollout restart deployment pinglink-frontend-depl --context do-tor1-pinglink-services'
                        }
                        
                    }
                }
                // stage('Test') {
                //     steps {
                //         nodejs(nodeJSInstallationName: 'nodejs') {
                //             sh 'cd auth && npm run test'

                           
                //         }
                //     }
                // }
            }
        }        
    }

    post {
        success {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'success'
            }
        }
        aborted {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'canceled'
            }
        }
        failure {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'failed'
            }
        }
    }
}

import { Schema, model } from "mongoose";

const schema = new Schema({
  userId: {
    type: String,
    required: true
  },
  endpoint: {
    type: String,
    required: true,
  },
})

const PushNotification = model('check', schema);

export { PushNotification }
export interface PushNotificationPayloadModel {
  notification: {
    title: string;
    body: string;
    icon: string;
    vibrate: [100, 50, 100];
    data: {
      dateOfArrival: number,
      primaryKey: 1
    },
    actions: [{
      action: 'explore',
      title: string,
    }]
  }
}
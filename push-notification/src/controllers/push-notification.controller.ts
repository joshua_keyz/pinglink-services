import {
  asyncHandler
} from '@pinglink/common';
import { Request, Response, NextFunction } from 'express';
import { PushNotification } from '../models/PushNotification';

export const addSubscriber = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
  const subscription = req.body.subscription;
  const token = (req as any).tokenData;
  const newNotification = new PushNotification({
    userId: token.id,
    endpoint: JSON.stringify(subscription),
  })
  // console.log('over here')
  await newNotification.save();
  res.json({success: true, msg: 'Subscription added successfully'});
});
import express from 'express';
import { router } from './routes/push-notification.routes';

const app = express();

app.use(express.json());

app.use('/api/push-notifications', router);

export { app };
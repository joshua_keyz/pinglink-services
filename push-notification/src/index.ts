import {
  connectDb,
  connectRabbit,
  errorHandler,
  rabbitWrapper,
} from '@pinglink/common';
import { app } from './app';
import { setVapidDetails } from 'web-push'
import { SendPushNotificationListener } from './events/send-push-notification.listener';
import { UserDeletedListener } from './events/user-deleted-notification.listener';

const vapidKeys = {
  publicKey: 'BJu7vLYx78JP-J83AVbs35HLe7uxAcOcZlwRHZ0efG6YToHopmr1_daxOutkUjGuhZi0kwFw2WZOeR7D8eJyGQ8',
  privateKey: process.env.PN_PRIVATE_KEY!,
};


setVapidDetails('mailto:joshua@pinglink.joshkeys.site', vapidKeys.publicKey, vapidKeys.privateKey);

connectDb(process.env.MONGODB_URI!, 'pinglink-push-notifications', 'Push Notifications Service');

connectRabbit(rabbitWrapper, 'amqp://rabbitmq-srv', () => {
  new SendPushNotificationListener(rabbitWrapper.client).listen();
  new UserDeletedListener(rabbitWrapper.client).listen();
});

app.use(errorHandler);

app.listen(3000, () => {
  console.log('Listening on port 3000!!!');
});

import { Listener, RabbitWrapper, VerifyEmailModel } from "@pinglink/common";
import { Connection } from "amqplib";
import { PushNotification } from "../models/PushNotification";
import { sendNotification } from "web-push";


export class SendPushNotificationListener extends Listener {
  subject = 'SEND_PUSH_NOTIFICATION';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    console.log('Handling push notifications)')
    const { notificationPayload, userId } = data;
    const userEndpoints = await PushNotification.find({userId}, {endpoint: 1, _id: 0});
    const webPushs =  userEndpoints.map((sub: any) => sendNotification(JSON.parse(sub.endpoint), JSON.stringify(notificationPayload)))
    try {
      await Promise.all(webPushs);
    } catch (err) {
      console.log('Error sending notification')
    }
  }

  constructor(public client: Connection){
    super(client)
  }
}
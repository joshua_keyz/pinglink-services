import { Listener } from "@pinglink/common";
import { Connection } from "amqplib";
import { PushNotification } from "../models/PushNotification";

export class UserDeletedListener extends Listener {
  subject = 'DELETE_USER_LISTENER';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    await PushNotification.findOneAndDelete({userId: data.id});
  }

  constructor(public client: Connection){
    super(client)
  }
}
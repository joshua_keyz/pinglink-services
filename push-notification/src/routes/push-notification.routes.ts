import { Router } from 'express';
import { addSubscriber } from '../controllers/push-notification.controller';
import { protect } from '@pinglink/common';

const router = Router();

router.route('/').post(protect(process.env.JWT_TOKEN!), addSubscriber);

export { router };
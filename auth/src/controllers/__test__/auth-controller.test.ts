import request from 'supertest';
import { app } from '../../app';

it('returns a 200 on successful request to /api/users/currentuser', async () => {
    return request(app)
    
        .get('/api/users/currentuser')
        .send()
        .expect(200);
});
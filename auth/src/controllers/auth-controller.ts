import { ErrorResponse, TokenModel, asyncHandler, rabbitWrapper } from "@pinglink/common";
import { NextFunction, Request, Response } from "express";
import { User } from "../models/UserSchema";
import { createHash } from 'crypto';
import { VerifyEmailPublisher } from "../events/verify-email-publisher";
import { ResetPasswordEmailPublisher } from "../events/reset-password-email-publisher";
import { DeleteUserPublisher } from "../events/delete-user-publisher";

export const currentUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const tokenData = (req as any).tokenData;

    const user = await User.findOne({email: tokenData.email})

    res.status(200).json({
        success: true,
        user
    })
});

export const createUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const { firstName, lastName, email, password } = req.body;
    let user = await User.findOne({email});

    if(user && user.verified) {
        return next(new ErrorResponse('User already exists', 400));
    }else if(user && !user.verified) {
        user.firstName = firstName;
        user.lastName = lastName;
        user.password = password;
    } else if(!user){
        user = await User.create({
            firstName,
            lastName,
            email,
            password,
            verified: false,
        });
    }


    const verificationCode = (user as any).getEmailVerificationToken();
    await user.save({validateBeforeSave: false});
    new VerifyEmailPublisher(rabbitWrapper.client).publish(JSON.stringify({
        email: user.email,
        verificationCode,
    }));

    res.send({
        success: true,
        token: (user as any).getSignedJwtToken(),
    });
});
export const setNewEmail = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const { email } = req.body;
    const tokenData: TokenModel = (req as any).tokenData;

    let user = await User.findOne({email});

    if(user && user.verified) {
        return next(new ErrorResponse('User already exists', 400));
    } 
        user = await User.findOne({email: tokenData.email});
    if(!user) {
        return next(new ErrorResponse('User not found', 404));
    }
    

    const verificationCode = (user as any).getEmailVerificationToken();
    user.incomingEmail = email;
    user.verified = false;
    await user.save({validateBeforeSave: false});

    new VerifyEmailPublisher(rabbitWrapper.client).publish(JSON.stringify({
        email,
        verificationCode,
    }));

    res.send({
        success: true,
    });
});

export const verifyNewEmail = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {

    const verificationToken = createHash("sha256")
    .update(req.body.verificationToken)
    .digest("hex");


    const verifiedUser = await User.findOne({
        verificationToken,
        // verificationTokenExpire: { $gt: Date.now() },
      });
    if(!verifiedUser) {
        return next(new ErrorResponse("Invalid token", 400));
    }
    verifiedUser.verified = true;
    verifiedUser.verificationToken = undefined;
    verifiedUser.verificationTokenExpire = undefined;
    verifiedUser.email = verifiedUser.incomingEmail!;
    verifiedUser.incomingEmail = undefined;
    await verifiedUser.save();

    res.status(200).json({
        success: true,
        newEmail: verifiedUser.email,
        token: (verifiedUser as any).getSignedJwtToken(),
    })
});

export const updateProfileDetails = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const { firstName, lastName } = req.body;
    const tokenData = (req as any).tokenData;
    const user = await User.findOne({email: tokenData.email})

    if(!user) {
        return next(new ErrorResponse('User not found', 404))
    }

    user.firstName = firstName;
    user.lastName = lastName;
    await user.save({validateBeforeSave: false});
    
    res.send({
        success: true,
        user
    });
});
export const getNewVerificationToken = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
    const userData = (req as any).tokenData;
    
    const user = await User.findOne({
        email: userData.email
    });

    if(!user) {
        return next(new ErrorResponse('Invalid token used to make this request', 400));
    }

    if(user.verified) {
        return next(new ErrorResponse('User already verified', 400))
    }

    const verificationCode = (user as any).getEmailVerificationToken();
    await user.save({validateBeforeSave: false});

    new VerifyEmailPublisher(rabbitWrapper.client).publish(JSON.stringify({
        email: user.email,
        verificationCode,
    }));

    res.send({
        success: true,
        token: (user as any).getSignedJwtToken(),
    });
})
export const verifyUser = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {

    const verificationToken = createHash("sha256")
    .update(req.body.verificationToken)
    .digest("hex");


    const verifiedUser = await User.findOne({
        verificationToken,
        // verificationTokenExpire: { $gt: Date.now() },
      });
    if(!verifiedUser) {
        return next(new ErrorResponse("Invalid token", 400));
    }
    verifiedUser.verified = true;
    verifiedUser.verificationToken = undefined;
    verifiedUser.verificationTokenExpire = undefined;
    await verifiedUser.save();

    res.status(200).json({
        success: true,
        token: (verifiedUser as any).getSignedJwtToken(),
    })
});

export const forgotPassword = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
    const user = await User.findOne({email: req.body.email});
    if(!user) {
        return next(new ErrorResponse('There is no user with that email', 404));
    }

    // Get reset token
    const resetToken = (user as any).getResetPasswordToken();
    new ResetPasswordEmailPublisher(rabbitWrapper.client).publish(JSON.stringify({
        email: user.email,
        resetPasswordCode: resetToken,
    }));
    await user.save({validateBeforeSave: false})
    res.status(200).json({
        success: true,
        data: user,
    })
})
export const verifyResetPasswordToken = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    // Get hashed token
    const resetPasswordToken = createHash("sha256")
      .update(req.body.resetToken)
      .digest("hex");
    console.log(resetPasswordToken);
    const user = await User.findOne({
      resetPasswordToken,
      resetPasswordExpire: { $gt: Date.now() },
    });

    if (!user) {
      return next(new ErrorResponse("Invalid token", 400));
    } else {
        return res.status(200).json({
            success: true
        });
    }
  }
);
export const deleteUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const tokenData: TokenModel = (req as any).tokenData;
    
    await User.deleteOne({email: tokenData.email});

    new DeleteUserPublisher(rabbitWrapper.client).publish(JSON.stringify({
        id: tokenData.id,
    }));

    res.status(200).send({
        success: true,
    })
})
export const changePassword = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
    const { oldPassword, newPassword } = req.body;
    const tokenData: TokenModel = (req as any).tokenData;

    let user;
    try {
        user = await User.findOne({email: tokenData.email}).select('+password');

    } catch(err) {
        return next(new ErrorResponse('Something Bad happend', 400))
    }

    if(!user) {
        return next(new ErrorResponse("Invalid user/password", 401));
    }

    if(!await (user as any).matchPassword(oldPassword)) {
        return next(new ErrorResponse("Invalid user/password", 401))
    }

    user.password = newPassword;
    await user.save({validateBeforeSave: false})

    res.json({
        success: true,
        token: (user as any).getSignedJwtToken(),
    })
});

export const resetPassword = asyncHandler(async(req: Request, res: Response, next: NextFunction) => {
    // Get hashed token
    const resetPasswordToken = createHash('sha256').update(req.body.resetToken).digest('hex');

    const user = await User.findOne({
        resetPasswordToken,
        resetPasswordExpire: {$gt: Date.now()}
    });

    if(!user) {
        return next(new ErrorResponse('Invalid token', 400));
    }
    console.log('over here')
    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
    await user.save();

    res.status(200).json({
        success: true,
        user,
        token: (user as any).getSignedJwtToken(),
    })
})


export const loginUser = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const { email, password } = req.body;
    let user: any;

    try {
        user = await User.findOne({email}).select('+password');

    } catch(err) {
        return next(new ErrorResponse('Something Bad happend', 400))
    }

    if(!user) {
        return next(new ErrorResponse("Invalid user/password", 401));
    }

    if(!await (user as any).matchPassword(password)) {
        return next(new ErrorResponse("Invalid user/password", 401))
    }

    res.json({
        success: true,
        token: (user as any).getSignedJwtToken(),
    })
});
export const verifyUserPassword = asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const { password } = req.body;

    const tokenData: TokenModel = (req as any).tokenData;
    const { email, id } = tokenData;
    let user: any;

    try {
        user = await User.findOne({email}).select('+password');
    } catch(err) {
        return next(new ErrorResponse('Something Bad happend', 400))
    }

    if(!user) {
        return next(new ErrorResponse("Invalid user", 401));
    }

    if(!await (user as any).matchPassword(password)) {
        return next(new ErrorResponse("Invalid password", 401))
    }

    res.json({
        success: true
    });
});
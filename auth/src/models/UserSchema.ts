import { compare, genSalt, hash } from "bcryptjs";
import { sign } from "jsonwebtoken";
import { createHash } from 'crypto';
import mongoose, { model } from "mongoose";

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'Please add a first name']
    },
    lastName: {
        type: String,
        required: [true, 'Please add a last name'],
    },
    email: {
        type: String,
        required: [true, 'Please add an email'],
        match: [/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, 'Please provide a valid email address'],
    },
    incomingEmail: {
        type: String,
        required: false,
        match: [/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, 'Please provide a valid email address'],
    },
    profilePicture: {
        type: String,
        required: false,
        default: 'https://pinglink-spaces.fra1.digitaloceanspaces.com/pinglink-assets/profile-placeholder.png',
    },
    password: {
        type: String,
        required: [true, 'Please add a password'],
        minlength: 6,
        select: false,
    },
    verified: {
        type: Boolean,
        default: false,
    },
    subscription: {
        type: String,
        default: 'freemium',
        enum: ['freemium', 'professional', 'enterprise']
    },
    verificationToken: String,
    verificationTokenExpire: Date,
    resetPasswordToken: String,
    resetPasswordExpire: Date,
    createAt: {
        type: Date,
        default: Date.now
    }
});

userSchema.pre('save', async function (next) {
    if(this.isModified('password')) {
        const salt = await genSalt(10);
        this.password = await hash(this.password!, salt);
    }
});

// Sign JWT and return
userSchema.methods.getSignedJwtToken = function() {
    return sign(
        {
            id: this._id,
            email: this.email,
            verified: this.verified,
        },
        process.env.JWT_TOKEN!
    );
};

// Generate a password token
userSchema.methods.getResetPasswordToken = function() {
    const randomDigits = Math.floor(100000 + Math.random() * 900000);
    this.resetPasswordToken = createHash('sha256').update(randomDigits.toString()).digest('hex');
    this.resetPasswordExpire = Date.now() + 60 * 60 * 1000;
    return randomDigits;
}
// Generate a email verification token
userSchema.methods.getEmailVerificationToken = function() {
    const randomDigits = Math.floor(100000 + Math.random() * 900000);
    this.verificationToken = createHash('sha256').update(randomDigits.toString()).digest('hex');
    this.verificationTokenExpire = Date.now() + 60 * 60 * 1000;
    return randomDigits;
}

userSchema.methods.matchPassword = async function(password: string) {
    const isMatched = await compare(password, this.password);
    return isMatched;
}

export const User = model('user', userSchema);
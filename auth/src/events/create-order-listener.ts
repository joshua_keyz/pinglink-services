import { Listener, RabbitWrapper, VerifyEmailModel } from "@pinglink/common";
import { Connection } from "amqplib";
import { User } from '../models/UserSchema';

export class CreateOrderListener extends Listener {
  subject = 'CREATE_ORDER_PUBLISHER';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    const {
      userId,
      subscription,
    } = data;
    await User.findByIdAndUpdate(userId, {subscription});
  }

  constructor(public client: Connection){
    super(client)
  }
}
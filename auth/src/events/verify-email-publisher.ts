import { Publisher } from "@pinglink/common";


export class VerifyEmailPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'VERIFY_EMAIL_REQUEST'
}

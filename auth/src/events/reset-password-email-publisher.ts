import { Publisher } from "@pinglink/common";


export class ResetPasswordEmailPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'RESET_PASSWORD_EMAIL_REQUEST';
}

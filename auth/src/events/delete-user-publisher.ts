import { Publisher } from "@pinglink/common";


export class DeleteUserPublisher extends Publisher {
  exchange = 'pinglink';
  subject = 'DELETE_USER_PUBLISHER'
}

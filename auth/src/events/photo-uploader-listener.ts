import { Listener, RabbitWrapper, VerifyEmailModel } from "@pinglink/common";
import { Connection } from "amqplib";
import { User } from '../models/UserSchema';

export class PhotoUploaderListener extends Listener {
  subject = 'PHOTO_UPLOADED_PUBLISHER';
  exchangeName = 'pinglink'

  onMessage = async (data: any) => {
    const {id, url} = data;
    await User.findByIdAndUpdate(id, {profilePicture: url});
  }

  constructor(public client: Connection){
    super(client)
  }
}
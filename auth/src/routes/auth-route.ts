import express from 'express';
import {
  changePassword,
  createUser,
  currentUser,
  deleteUser,
  forgotPassword,
  getNewVerificationToken,
  loginUser,
  resetPassword,
  setNewEmail,
  updateProfileDetails,
  verifyNewEmail,
  verifyResetPasswordToken,
  verifyUser,
  verifyUserPassword,
} from "../controllers/auth-controller";
import { protect } from '@pinglink/common';

const authRouter = express.Router();
authRouter.route('/signup').post(createUser)
authRouter.route('/signin').post(loginUser)
authRouter.route('/update-profile-details').post(protect(process.env.JWT_TOKEN!), updateProfileDetails);
authRouter.route('/current-user').get(protect(process.env.JWT_TOKEN!), currentUser)
authRouter.route('/forgot-password').post(forgotPassword);
authRouter.route('/reset-password').put(resetPassword);
authRouter.route('/verify-email').post(verifyUser)
authRouter.route('/resend-verification-code').post(protect(process.env.JWT_TOKEN!), getNewVerificationToken)
authRouter.route('/verify-reset-password-token').post(verifyResetPasswordToken);
authRouter.route('/verify-password').post(protect(process.env.JWT_TOKEN!), verifyUserPassword);
authRouter.route('/set-new-email').post(protect(process.env.JWT_TOKEN!), setNewEmail);
authRouter.route('/verify-new-email').post(protect(process.env.JWT_TOKEN!), verifyNewEmail);
authRouter.route('/change-password').post(protect(process.env.JWT_TOKEN!), changePassword);
authRouter.route('/delete-user').delete(protect(process.env.JWT_TOKEN!), deleteUser);

export { authRouter };
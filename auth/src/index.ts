import {
  connectDb,
  connectRabbit,
  errorHandler,
  rabbitWrapper,
  RabbitWrapper,
} from '@pinglink/common';
import { app } from './app';
import { PhotoUploaderListener } from './events/photo-uploader-listener';
import { CreateOrderListener } from './events/create-order-listener';

connectDb(process.env.MONGODB_URI!, 'pinglink-auth', 'Auth Service');
connectRabbit(rabbitWrapper, 'amqp://rabbitmq-srv', () => {
  new PhotoUploaderListener(rabbitWrapper.client).listen();
  new CreateOrderListener(rabbitWrapper.client).listen();
});
app.use(errorHandler);

app.listen(3000, () => {
  console.log('Listening on port 3000!!!!');
});

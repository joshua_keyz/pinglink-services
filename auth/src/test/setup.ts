import mongoose from 'mongoose';
import { app } from '../app';

let mongo: any ;

beforeAll(async () => {
    const url = process.env.MONGODB_URI!
    mongo = await mongoose.connect(url, {dbName: 'test'})
});

beforeEach(async () => {
    const collections = await mongoose.connection.db.collections();

    for(let collection of collections) {
        await collection.deleteMany({});
    }
});

afterAll(async () => {
    if() {
        await mongo.stop();
    }
    await mongoose.connection.close();
});